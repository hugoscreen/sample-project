<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class RolesPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Clean data tables
        DB::table('model_has_roles')->delete();
        DB::table('role_has_permissions')->delete();
        DB::table('permissions')->delete();
        DB::table('roles')->delete();

        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'admin users']);
        Permission::create(['name' => 'admin roles']);
        Permission::create(['name' => 'admin permissions']);

        Permission::create(['name' => 'create clients']);
        Permission::create(['name' => 'read clients']);
        Permission::create(['name' => 'edit clients']);
        Permission::create(['name' => 'delete clients']);

        Permission::create(['name' => 'create positions']);
        Permission::create(['name' => 'read positions']);
        Permission::create(['name' => 'edit positions']);
        Permission::create(['name' => 'delete positions']);

        Permission::create(['name' => 'create live_interviews']);
        Permission::create(['name' => 'read live_interviews']);
        Permission::create(['name' => 'edit live_interviews']);
        Permission::create(['name' => 'delete live_interviews']);

        Permission::create(['name' => 'create virtual_interviews']);
        Permission::create(['name' => 'read virtual_interviews']);
        Permission::create(['name' => 'edit virtual_interviews']);
        Permission::create(['name' => 'delete virtual_interviews']);

        Permission::create(['name' => 'create questions']);
        Permission::create(['name' => 'read questions']);
        Permission::create(['name' => 'edit questions']);
        Permission::create(['name' => 'delete questions']);

        Permission::create(['name' => 'create answers']);
        Permission::create(['name' => 'read answers']);
        Permission::create(['name' => 'edit answers']);
        Permission::create(['name' => 'delete answers']);

        Permission::create(['name' => 'create documents']);
        Permission::create(['name' => 'read documents']);
        Permission::create(['name' => 'edit documents']);
        Permission::create(['name' => 'delete documents']);

        Permission::create(['name' => 'create comments']);
        Permission::create(['name' => 'read comments']);
        Permission::create(['name' => 'edit comments']);
        Permission::create(['name' => 'delete comments']);

        // create roles and assign created permissions
        $role = Role::create(['name' => 'applicant'])
            ->givePermissionTo(
                [
                    'read clients',
                    'read positions',
                    'read live_interviews',
                    'edit live_interviews',
                    'read virtual_interviews',
                    'edit virtual_interviews',
                    'read questions',
                    'create answers',
                    'read answers',
                    'edit answers',
                    'delete answers',
                    'create documents',
                    'read documents',
                    'edit documents',
                    'delete documents'
                ]
            );

        $role = Role::create(['name' => 'recruiter'])
            ->givePermissionTo(
                [
                    'create clients',
                    'read clients',
                    'edit clients',
                    'delete clients',
                    'create positions',
                    'read positions',
                    'edit positions',
                    'delete positions',
                    'create live_interviews',
                    'read live_interviews',
                    'edit live_interviews',
                    'delete live_interviews',
                    'create virtual_interviews',
                    'read virtual_interviews',
                    'edit virtual_interviews',
                    'delete virtual_interviews',
                    'create questions',
                    'read questions',
                    'edit questions',
                    'delete questions',
                    'create documents',
                    'read documents',
                    'edit documents',
                    'delete documents'
                ]
            );

        $role = Role::create(['name' => 'contractor'])
            ->givePermissionTo(
                [
                    'read clients',
                    'read positions',
                    'read live_interviews',
                    'edit live_interviews',
                    'read virtual_interviews',
                    'edit virtual_interviews',
                    'read questions',
                    'read answers',
                    'read documents',
                    'create comments',
                    'read comments',
                    'edit comments',
                    'delete comments'
                ]
            );

        $role = Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());
    }
}
