<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Clean data tables
        // DB::table('languages')->delete(); // enable for start from clean database

        $skills = array(
            array('name' => 'English'),
            array('name' => 'French'),
            array('name' => 'Mandarin'),
            array('name' => 'Portuguese'),
            array('name' => 'Spanish')
        );
        DB::table('languages')->insert($skills);

    }
}
