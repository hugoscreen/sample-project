<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CanadaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //seeding countries
        $countries = array(
            array('id' => 3, 'code' => 'CA', 'name' => 'Canada'),
        );
        DB::table('countries')->insert($countries);

        //seeding Canada states
        $states = array(
            array('name' => "Alberta", 'country_id' => 3),
            array('name' => "British Columbia", 'country_id' => 3),
            array('name' => "Manitoba", 'country_id' => 3),
            array('name' => "New Brunswick", 'country_id' => 3),
            array('name' => "Newfoundland and Labrador", 'country_id' => 3),
            array('name' => "Northwest Territories", 'country_id' => 3),
            array('name' => "Nova Scotia", 'country_id' => 3),
            array('name' => "Nunavut", 'country_id' => 3),
            array('name' => "Ontario", 'country_id' => 3),
            array('name' => "Prince Edward Island", 'country_id' => 3),
            array('name' => "Quebec", 'country_id' => 3),
            array('name' => "Saskatchewan", 'country_id' => 3),
            array('name' => "Yukon", 'country_id' => 3),
        );
        DB::table('states')->insert($states);
    }
}
