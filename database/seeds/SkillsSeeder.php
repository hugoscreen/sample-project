<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class SkillsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Clean data tables
        // DB::table('skills')->delete(); // enable for start from clean database

        $skills = array(
            array('name' => '.NET'),
            array('name' => 'ANDROID'),
            array('name' => 'ANGULAR'),
            array('name' => 'AUTOMATED TESTING'),
            array('name' => 'C#'),
            array('name' => 'IOS'),
            array('name' => 'JAVA'),
            array('name' => 'JAVASCRIPT'),
            array('name' => 'LARAVEL'),
            array('name' => 'MANUAL TESTING'),
            array('name' => 'MONGO DB'),
            array('name' => 'MS SQL'),
            array('name' => 'PHP'),
            array('name' => 'PYTHON'),
            array('name' => 'REACT'),
            array('name' => 'REACT NATIVE'),
            array('name' => 'UI or UX DESIGN')
        );
        DB::table('skills')->insert($skills);

    }
}
