<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesPermissionsSeeder::class);
        $this->call(CountriesStatesSeeder::class);
        $this->call(LanguagesSeeder::class);
        $this->call(SkillsSeeder::class);
        $this->call(CanadaSeeder::class);
    }
}
