<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->dropForeign('comments_contractor_id_foreign');
            $table->dropColumn('contractor_id');
            $table->dropForeign('comments_liveinterview_id_foreign');
            $table->dropColumn('liveinterview_id');
        });

        Schema::table('comments', function (Blueprint $table) {
            $table->foreignId('contractor_id')->nullable()->constrained();
            $table->foreignId('liveinterview_id')->nullable()->constrained();
            $table->foreignId('recruiter_id')->nullable()->constrained();
            $table->foreignId('ondemandinterview_id')->nullable()->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->dropForeign('comments_contractor_id_foreign');
            $table->dropColumn('contractor_id');
            $table->dropForeign('comments_liveinterview_id_foreign');
            $table->dropColumn('liveinterview_id');
        });

        Schema::table('comments', function (Blueprint $table) {
            $table->foreignId('contractor_id')->constrained();
            $table->foreignId('liveinterview_id')->constrained();
        });
    }
}
