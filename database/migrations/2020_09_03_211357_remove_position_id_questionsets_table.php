<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemovePositionIdQuestionsetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questionsets', function (Blueprint $table) {
            $table->dropForeign('questionsets_position_id_foreign');
            $table->dropColumn('position_id');
        });
    }
}
