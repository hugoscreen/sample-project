<?php

use App\Questionset;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdInQuestionsetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questionsets', function (Blueprint $table) {
            $table->boolean('is_public')->default(false);
            $table->foreignId('user_id')->default(1)->constrained();
        });

        Questionset::all()->toQuery()->update(['is_public' => true]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questionsets', function (Blueprint $table) {
            $table->dropColumn('is_public');
            $table->dropForeign('questionsets_user_id_foreign');
            $table->dropColumn('user_id');
        });
    }
}
