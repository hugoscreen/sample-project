<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPositionToLiveinterviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('liveinterviews', function (Blueprint $table) {
            $table->dropForeign(['applicant_id']);
            $table->dropColumn('applicant_id');
            $table->foreignId('application_id')->constrained();
            $table->text('BBBinternalId')->nullable();
            $table->text('internalMeetingId')->nullable();
            $table->dateTime('scheduled')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('liveinterviews', function (Blueprint $table) {
            $table->dropForeign(['application_id']);
            $table->dropColumn('application_id');
            $table->dropColumn('BBBinternalId');
            $table->dropColumn('internalMeetingId');
            $table->dropColumn('scheduled');
        });
    }
}
