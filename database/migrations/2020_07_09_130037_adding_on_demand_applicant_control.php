<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddingOnDemandApplicantControl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ondemandinterviews', function (Blueprint $table) {
            $table->boolean('openedByApplicant')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ondemandinterviews', function (Blueprint $table) {
            $table->dropColumn('openedByApplicant');
        });
    }
}
