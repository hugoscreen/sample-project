<?php

use App\Position;
use App\Questionset;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePositionQuestionsetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('position_questionset', function (Blueprint $table) {
            $table->id();
            $table->foreignId('position_id')->constrained();
            $table->foreignId('questionset_id')->constrained();
            $table->timestamps();
        });

        $positions = Position::all();
        foreach ($positions as $position) {
            $questionsets = Questionset::where('position_id', $position->id)->get();
            $position->questionsets()->sync($questionsets);
            $position->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_questionset');
    }
}
