<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddComplementaryFieldsToCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->foreignId('state_id')->nullable()->after('phone')->constrained();
            $table->string('city', 150)->nullable()->after('state_id');
            $table->string('logo', 300)->nullable()->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropForeign(['state_id']);
            $table->dropColumn(
                array(
                    'state_id',
                    'city',
                    'logo'
                )
            );
        });
    }
}
