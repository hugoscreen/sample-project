<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShowHiringManagerToLiveinterviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('liveinterviews', function (Blueprint $table) {
            $table->boolean('show_hiring_manager')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('liveinterviews', function (Blueprint $table) {
            $table->dropColumn('show_hiring_manager');
        });
    }
}
