<?php

namespace App;

use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\QuestionsetController;

class BBB
{
    const METING_PASSWORDS = [
        'moderator' => 'TO39vGPpOQtmPF8wtG9JwqxwCk30',
        'attendee' => 'UNwdR8eg0oc3EUfDpw9mmlsfdCHT'
    ];

    public function createRoom($roomId, $position, $questionset_id = null, $autoStartrecording = false, $rehearsalRoom = false)
    {
        $bbb = new BigBlueButton();

        $createMeetingParams = new CreateMeetingParameters($roomId, $position->name);
        //access to the meeting
        $createMeetingParams->setAttendeePassword(self::METING_PASSWORDS['attendee']);
        $createMeetingParams->setModeratorPassword(self::METING_PASSWORDS['moderator']);
        //customization look and feel
        $createMeetingParams->setLogo("http://tas-screenit.com/wp-content/themes/screenit-v2/imagenes/logo.png");
        $createMeetingParams->setLockSettingsDisablePublicChat(false);
        $createMeetingParams->setLockSettingsDisableNote(true);
        $createMeetingParams->setModeratorOnlyMessage('');
        $createMeetingParams->setWelcomeMessage('');

        if ($rehearsalRoom) {
            $createMeetingParams->addPresentation('https://collokio.sfo2.cdn.digitaloceanspaces.com/staticElements/rehearsalroom.pdf');
        } else if($questionset_id) {
            $presentation = Storage::disk('do')->url(
                'position/'.$position->id.'/questionsets/'.$questionset_id.'.pdf'
            );
            $presentation = str_replace("digitaloceanspaces","cdn.digitaloceanspaces", $presentation);
            $createMeetingParams->addPresentation($presentation);
        } else {
            $createMeetingParams->addPresentation('https://res.cloudinary.com/hunab/image/upload/v1590162268/PDF_1_smbgjx.pdf');
        }

        // If the room is for a rehearsal, it won't recording the meeting
        $createMeetingParams->setRecord(!$rehearsalRoom);
        $createMeetingParams->setAllowStartStopRecording(!$rehearsalRoom);
        if ($autoStartrecording){
            $createMeetingParams->setAutoStartRecording(true);
        }

        //handle the redirect after logout
        $createMeetingParams->setLogoutUrl(env('BBB_FINISH_URL'));
        $response = $bbb->createMeeting($createMeetingParams);
        return $response->getInternalMeetingId();
    }

    public function joinMeeting($meetingId, $user, $ondemand = false, $rehearsalRoom = false)
    {

        $bbb = new BigBlueButton();

        if($user->hasRole('applicant')){
            if($ondemand) {
                $joinParams = new JoinMeetingParameters($meetingId, $user->getAttribute('name'), self::METING_PASSWORDS['moderator']);
                if(!$rehearsalRoom) {
                    $interview = Ondemandinterview::where('internalMeetingId', $meetingId)->first();
                    $interview->setAttribute('openedByApplicant', 1);
                    $interview->save();
                }
            } else {
                $joinParams = new JoinMeetingParameters($meetingId, $user->getAttribute('name'), self::METING_PASSWORDS['attendee']);
            }
        } else {
            $joinParams = new JoinMeetingParameters($meetingId, $user->getAttribute('name'), self::METING_PASSWORDS['moderator']);
        }

        $joinParams->setRedirect(true);
        $joinParams->setJoinViaHtml5(true);
        $joinParams->setCustomParameter('userdata-bbb_show_participants_on_login', 'false');
        $joinParams->setCustomParameter('userdata-bbb_listen_only_mode', 'false');
        $joinParams->setCustomParameter('userdata-bbb_auto_join_audio', 'true');
        $array['data']= ['message'=>'Successful url fetch for live join', 'meetingURL'=>$bbb->getJoinMeetingURL($joinParams)];
        return $array;
    }
}
