<?php

namespace App;

use App\Http\Controllers\BBBController;
use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\GetMeetingInfoParameters;
use BigBlueButton\Parameters\GetRecordingsParameters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class Ondemandinterview extends Model
{
    use SoftDeletes;
    const RUNNING = 'running';
    const ENDED_INTERVIEW = 'ended';
    const NOT_STARTED_INTERVIEW = 'not_started';

    public function application()
    {
        return $this->belongsTo('App\Application');
    }

    public function questionset()
    {
        return $this->belongsTo('App\Questionset');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function videolistings()
    {
        return $this->morphToMany('App\VideoListing', 'videolistables', 'videolistable_type', 'videolistable_id');
    }

    public function meetingStatus()
    {
        if($this->getAttribute('status') === self::ENDED_INTERVIEW){
            return self::ENDED_INTERVIEW;
        }

        $bbb = new BigBlueButton();
        $getMeetingParams = new GetMeetingInfoParameters($this->getAttribute('internalMeetingId'), BBB::METING_PASSWORDS['moderator']);
        $response = $bbb->getMeetingInfo($getMeetingParams);
        if($response->success()){

            if($this->getAttribute('openedByApplicant')) {
                $this->setAttribute('status',self::RUNNING);
                $this->save();
                return self::RUNNING;
            }
            $this->setAttribute('status',self::NOT_STARTED_INTERVIEW);
            $this->save();
            return self::NOT_STARTED_INTERVIEW;
        }

        $this->setAttribute('status',self::ENDED_INTERVIEW);
        $this->save();
        return self::ENDED_INTERVIEW;

    }

    public function getLastVideo()
    {
        $video = null;
        if ($this->getAttribute('status') === self::ENDED_INTERVIEW){
            $video=env('BBB_SERVER_PRESENTATION_URL').$this->getAttribute('BBBinternalId').'/video/webcams.webm#t=30';
        }
        return $video;
    }

    public function getRecording()
    {
        $bbb = new BigBlueButton();
        $recordingParams = new GetRecordingsParameters();
        $recordingParams->setMeetingId($this->internalMeetingId);
        $response = $bbb->getRecordings($recordingParams);

        if(empty($response->getRecords()) || empty($this->getAttribute('internalMeetingId'))){
            return 'No recordings available';
        }

        $xmlArray = json_decode(json_encode($response->getRawXml()), true);
        if (!empty($xmlArray)) {
            $recording = end($xmlArray['recordings']);
        }
        return empty($recording) ? '' : $recording['playback']['format']['url'];
    }
}
