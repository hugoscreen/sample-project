<?php

namespace App;

use App\Questionset;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Questionset extends Model
{
    use SoftDeletes;

    protected $casts = [
        'is_public' => 'boolean',
    ];

    public function scopePublic($builder)
    {
        $user = Auth::user();
        /**
         * If the user is not a super-admin, the questions sets should be only
         * the owned questionset or the questionsets publics
         */
        if (!empty($user) && !$user->hasRole('super-admin')) {
            $builder->where(function($query) {
                $user = Auth::user();
                $query->where('user_id', $user->id)
                ->orWhere('is_public', true);
            });
        }
    }

    public function positions()
    {
        return $this->belongsToMany('App\Position', 'position_questionset');
    }

    public function questions()
    {
        return $this->hasMany('App\Question', 'questionset_id')->orderBy('order');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
