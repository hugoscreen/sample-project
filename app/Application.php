<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends Model
{
    use SoftDeletes;

    public function applicant()
    {
        return $this->belongsTo('App\Applicant', 'applicant_id');
    }

    public function position()
    {
        return $this->belongsTo('App\Position', 'position_id');
    }

    public function liveinterviews()
    {
        return $this->hasMany('App\Liveinterview');
    }

    public function ondemandinterviews()
    {
        return $this->hasMany('App\Ondemandinterview');
    }

    public function documents()
    {
        return $this->hasMany('App\Document');
    }

    public function skills()
    {
        return $this->belongsToMany('App\Skill', 'application_skill')
            ->withPivot('contractor_id', 'evaluation');
    }

    public function languages()
    {
        return $this->belongsToMany('App\Language', 'application_language')
            ->withPivot('contractor_id', 'evaluation');
    }
}
