<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShareableLink extends Model
{
    public function shareable()
    {
        return $this->morphTo();
    }
}
