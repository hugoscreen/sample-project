<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Company extends Model
{
    use SoftDeletes;

    protected $table = 'companies';


    public function contact()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function state()
    {
        return $this->belongsTo('App\State', 'state_id');
    }

    public function positions()
    {
        return $this->hasMany('App\Position', 'company_id');
    }

    public function contractors()
    {
        return $this->hasMany('App\Contractor');
    }

    public function applications()
    {
        return $this->hasManyThrough('App\Application', 'App\Position', 'company_id', 'position_id', 'id', 'id');
    }

    public function country()
    {
        return $this->hasOneThrough('App\Country', 'App\State', 'id', 'id', 'state_id', 'country_id');
    }

    public function profileImageLink()
    {
        if (Storage::disk('do')->missing($this->getAttribute('profile_picture'))) {
            $this->setAttribute('profile_picture', null);
            return 'File is missing';
        }

        return Storage::disk('do')->temporaryUrl(
            $this->getAttribute('profile_picture'), now()->addMinutes(2)
        );
    }
}
