<?php

namespace App\Http\Controllers;

use App\Email;
use App\Http\Requests\ResepasswordUserRequest;
use App\User;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\ResendRegistrationEmailRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserCollection;
use App\State;
use App\Notification;
use App\Events\NewNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\User as UserResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

/**
 * @group User Controller
 *
 * <aside>APIs for managing User model</aside>
 */
class UserController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(User::class);
    }

    public static function createUser($request) {
        $user = new User();
        $user->setAttribute('name', $request->get('name'));
        $user->setAttribute('email', $request->get('email'));
        if(!is_null($request->get('phone'))){
            $user->setAttribute('phone', $request->get('phone'));
        }
        $password = Str::random(8);
        $user->setAttribute('password', Hash::make($password));
        $user->save();

        $user->unhashPassword = $password;
        if ($request->hasFile('profile_picture')) {
            $file = $request->file('profile_picture');
            $name = date('Y-m-d-H-i-s-').uniqid();
            $user->setAttribute('profile_picture', 'users/'.$user->id.'/'.$name.'.'.$file->getClientOriginalExtension());
            $file->storeAs('users/'.$user->id, $name.'.'.$file->getClientOriginalExtension(), 'do');
            $user->save();
        }
        return $user;
    }

    /**
     * User Index Method
     *
     * <aside>Get a list of all the User in storage.</aside>
     * @authenticated
     *
     * @responseField data array User Collection Resource
     *
     * @apiResourceCollection App\Http\Resources\UserCollection
     * @apiResourceModel App\User
     */
    public function index()
    {
        return new UserCollection(User::all());
    }

    /**
     * User Create Method
     *
     * <aside>Create a new User entry in storage</aside>
     * @authenticated
     *
     * @bodyParam profile_picture image The profile picture for the user.
     *
     * @responseField data object User Resource
     *
     * @apiResource App\Http\Resources\User
     * @apiResourceModel App\User
     */
    public function store(StoreUserRequest $request)
    {
        $user = new User();
        $user->setAttribute('name', $request->get('name'));
        $user->setAttribute('email', $request->get('email'));
        if(!is_null($request->get('phone'))){
            $user->setAttribute('phone', $request->get('phone'));
        }
        $password = Str::random(8);
        $user->setAttribute('password', Hash::make($password));
        $user->save();

        if ($request->hasFile('profile_picture')) {
            $file = $request->file('profile_picture');
            $name = date('Y-m-d-H-i-s-').uniqid();
            $user->setAttribute('profile_picture', 'users/'.$user->id.'/'.$name.'.'.$file->getClientOriginalExtension());
            $file->storeAs('users/'.$user->id, $name.'.'.$file->getClientOriginalExtension(), 'do');
            $user->save();
        }

        //generating notification
        $email = new Email();
        $email->user_id = $user->id;
        $email->url = env('FRONT_URL', 'https://collok.io')."/login?email=".$user->email;
        $email->subject = "Collok.io - New User Credetials";
        $email->message = 'Hello, '. $user->name.':<br/><br/>';
        $email->message .= 'You have received an email from the Collok.io platform.<br/><br/>';
        $email->message .= "The system has generated a user for you, you can access with the following data:<br/>";
        $email->message .= "Username: {$user->email}<br/>";
        $email->message .= "Pasword: {$password}<br/>";
        // TODO: generate persistent emails
        $email->sendMail(false);

        return (new UserResource($user))->additional(['message' => 'User created successfully']);
    }

    /**
     * User Show Method
     *
     * <aside>Get info from the specified User in storage.</aside>
     * @authenticated
     *
     * @urlParam user required The ID of the User.
     *
     * @responseField data object User Resource
     *
     * @apiResource App\Http\Resources\User
     * @apiResourceModel App\User
     */
    public function show(User $user)
    {
        $user->roles;
        if ($user->hasRole('contractor')) {
            $user->load('contractor');
        }
        if ($user->hasRole('recruiter')) {
            $user->load('recruiter');
        }
        if ($user->hasRole('applicant')) {
            $user->load('applicant');
        }
        return new UserResource($user);
    }

    /**
     * User Update Method
     *
     * <aside>Update the specified User's info in storage. To update or upload a profile picture (file),
     * the request should change to POST and add a extra bodyparam called "_method" with value "PUT"</aside>
     *
     * @authenticated
     * @urlParam user required The ID of the User.
     *
     * @bodyParam password_confirmation string
     * @bodyParam profile_picture image The profile picture for the user.
     *
     * @responseField data object User Resource
     *
     * @apiResource App\Http\Resources\User
     * @apiResourceModel App\User
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        if(!is_null($request->get('name'))){
            $user->setAttribute('name', $request->get('name'));
        }

        if(!is_null($request->get('email'))){
            $user->setAttribute('email', $request->get('email'));
        }

        if(!is_null($request->get('phone'))){
            $user->setAttribute('phone', $request->get('phone'));
        }

        if(!is_null($request->get('password'))){
            $user->setAttribute('password', Hash::make($request->get('password')));
        }

        if ($request->hasFile('profile_picture')) {
            if (!empty($user->getAttribute('profile_picture'))) {
                if (Storage::disk('do')->exists($user->getAttribute('profile_picture'))) {
                    Storage::disk('do')->delete($user->getAttribute('profile_picture'));
                }
            }
            $file = $request->file('profile_picture');
            $name = date('Y-m-d-H-i-s-').uniqid();
            $user->setAttribute('profile_picture', 'users/'.$user->id.'/'.$name.'.'.$file->getClientOriginalExtension());
            $file->storeAs('users/'.$user->id, $name.'.'.$file->getClientOriginalExtension(), 'do');
        }

        $user->save();

        return (new UserResource($user))->additional(['message' => 'User updated successfully']);
    }

    /**
     * User Delete Method
     *
     * <aside>Remove the specified User from storage.</aside>
     * @authenticated
     *
     * @urlParam user required The ID of the User.
     *
     * @response {
     *  "message": "User deleted successfully"
     * }
     */
    public function destroy(User $user)
    {
        $user->delete();
        return response()->json([
            'message' => 'User deleted successfully'
        ]);
    }


    /**
     * Me Method
     * <aside>Get info from the user-owned.</aside>
     * @authenticated
     *
     * @responseField data object User Resource
     *
     * @apiResource App\Http\Resources\User
     * @apiResourceModel App\User
     */
    public function me(Request $request)
    {
        $user = $request->user();
        $user->roles;
        if ($user->hasRole('contractor')) {
            $user->load('contractor');
            $user->load('contractor.company');
        }
        if ($user->hasRole('recruiter')) {
            $user->load('recruiter');
        }
        if ($user->hasRole('applicant')) {
            $user->load('applicant');
        }
        return new UserResource($user);
    }


    /**
     * Reset Password Method
     *
     * <aside>Resent the password for a new random one and send a mail notification to the user.</aside>
     *
     * @responseField data object User Resource
     *
     * @apiResource App\Http\Resources\User
     * @apiResourceModel App\User
     */
    public function password_reset(ResepasswordUserRequest $request){
        $user = User::where('email',$request->get('email'))->firstOrFail();
        $password = Str::random(8);
        $user->setAttribute('password', Hash::make($password));
        $user->save();

        //generate notification
        $email = new Email();
        $email->user_id = $user->id;
        $email->url = env('FRONT_URL', 'https://collok.io')."/login?email=".$user->email;
        $email->subject = "Collok.io - Password Reset ";
        $email->message = 'Hello '. $user->name.':<br/><br/>';
        $email->message .= 'You have received an email from the Collok.io platform.<br/><br/>';
        $email->message .= "You have completed the password reset process. Your new password is:<br/>";
        $email->message .= "Pasword: {$password}<br/>";
        $email->sendMail(false);

        $notification = new Notification();
        $notification->setAttribute('title', 'Password Reset');
        $notification->setAttribute('text', 'Password reset was requested, verify your email.');
        $notification->setAttribute('type', 'warning');
        $notification->user()->associate($user);
        $notification->save();
        broadcast(new NewNotification($notification));
    }

    /**
     * Resend Registration Mail Method
     *
     * <aside>Resent the registration mail to the user specified in the body parameter.
     * This method update the password for a new random one.</aside>
     *
     * @responseField data object User Resource
     *
     * @apiResource App\Http\Resources\User
     * @apiResourceModel App\User
     */
    public function resendRegistrationEmail (ResendRegistrationEmailRequest $request) {
        $this->authorize('resendRegistrationEmail', Auth::user());
        $validated = $request->validated();
        $user = User::where('id',$validated['user_id'])->firstOrFail();
        $password = Str::random(8);
        $user->setAttribute('password', Hash::make($password));
        $user->save();

        $email = new Email();
        if($user->hasRole('applicant')){
            $email->template = 'new-aplicant';
        }

        if($user->hasRole('recruiter')){
            $email->template = 'new_user_recruiter';
        }

        if($user->hasRole('contractor')){
            $email->user_id = $user->id;
            $email->url = env('FRONT_URL', 'https://collok.io').'/login?email='.$user->email;
            $email->subject = 'Collok.io - New User Credetials';
            $email->message = 'The system has generated a user for you, you can access with the following data:<br/>';
            $email->message .= 'Username: '.$user->email.'<br/>';
            $email->message .= 'Pasword: '.$password.'<br/>';
            // TODO: generate persistent emails
            $email->sendMail(false);
        }

        $email->user_id = $user->id;
        $email->subject = 'Welcome to Collok.io';

        $email->data = [
            'name' => $user->name,
            'mail' => $user->email,
            'password' => $password
        ];

        if(!empty($email->template)){
            $email->sendMail();
        }

        return (new UserResource($user))->additional(['message' => 'The registration email has been sent again']);
    }
}
