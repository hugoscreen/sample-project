<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\VideoListing;
use App\Liveinterview;
use App\Ondemandinterview;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreShareableLinkRequest;
use App\Http\Resources\VideoListingCollection;
use App\Http\Requests\StoreVideoListingRequest;
use App\Http\Requests\UpdateVideoListingRequest;
use App\Http\Resources\VideoListing as VideoListingResource;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;

/**
 * @group Video Listing Controller
 *
 * <aside>APIs for managing Video Listing model</aside>
 */
class VideoListingController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(VideoListing::class);
    }

    /**
     * Video Listing Index Method
     * 
     * <aside>Get a list of all the Video Listing in storage.</aside>
     * @authenticated
     * 
     * @responseField data array Video Listing Collection Resource
     * 
     * @apiResourceCollection App\Http\Resources\VideoListingCollection
     * @apiResourceModel App\VideoListing
     */
    public function index()
    {
        $user = Auth::user();

        if (!$user->hasRole('super-admin') && !$user->hasRole('recruiter')) {
            abort(403, 'Unauthorized action');
        }

        if ($user->hasRole('super-admin')) {
            $videoListings = VideoListing::all();
        } else if ($user->hasRole('recruiter')) {
            $videoListings = VideoListing::where('user_id', $user->id)->get();
        }

        return new VideoListingCollection($videoListings);
    }

    /**
     * Video Listing Create Method
     * 
     * <aside>Create a new Video Listing entry in storage</aside>
     * @authenticated
     * 
     * @responseField data object Video Listing Resource
     * 
     * @apiResource App\Http\Resources\VideoListing
     * @apiResourceModel App\VideoListing
     */
    public function store(StoreVideoListingRequest $request)
    {
        $user = Auth::user();

        $videoListing = new VideoListing();
        $videoListing->setAttribute('name', $request->get('name'));
        if(!is_null($request->get('description'))){
            $videoListing->setAttribute('description', $request->get('description'));
        }
        $videoListing->user()->associate($user);
        $videoListing->save();


        if (!is_null($request->get('liveinterviews'))) {
            $liveInterviews =  Liveinterview::findOrFail($request->get('liveinterviews'));
            $videoListing->liveinterviews()->sync($liveInterviews);
        }

        if (!is_null($request->get('ondemandinterviews'))) {
            $ondemandInterviews =  Ondemandinterview::findOrFail($request->get('ondemandinterviews'));
            $videoListing->ondemandinterviews()->sync($ondemandInterviews);
        }

        return (new VideoListingResource($videoListing))->additional(['message' => 'VideoListing created successfully']);
    }

    /**
     * Video Listing Show Method
     * 
     * <aside>Get info from the specified Video Listing in storage.</aside>
     * @authenticated
     *
     * @urlParam video_listing required The ID of the Video Listing.
     * 
     * @responseField data object Video Listing Resource
     * 
     * @apiResource App\Http\Resources\VideoListing
     * @apiResourceModel App\VideoListing
     */
    public function show(VideoListing $videoListing)
    {
        return new VideoListingResource($videoListing);
    }

    /**
     * Video Listing Update Method
     * 
     * <aside>Update the specified Video Listing's info in storage.</aside>
     * 
     * @authenticated
     * @urlParam video_listing required The ID of the Video Listing.
     * 
     * @responseField data object Video Listing Resource
     * 
     * @apiResource App\Http\Resources\VideoListing
     * @apiResourceModel App\VideoListing
     */
    public function update(UpdateVideoListingRequest $request, VideoListing $videoListing)
    {
        if(!is_null($request->get('name'))){
            $videoListing->setAttribute('name', $request->get('name'));
        }

        if(!is_null($request->get('description'))){
            $videoListing->setAttribute('description', $request->get('description'));
        }

        $videoListing->save();

        if (!is_null($request->get('liveinterviews'))) {
            $liveInterviews =  Liveinterview::findOrFail($request->get('liveinterviews'));
            $videoListing->liveinterviews()->sync($liveInterviews);
        }

        if (!is_null($request->get('ondemandinterviews'))) {
            $ondemandInterviews =  Ondemandinterview::findOrFail($request->get('ondemandinterviews'));
            $videoListing->ondemandinterviews()->sync($ondemandInterviews);
        }

        return (new VideoListingResource($videoListing))->additional(['message' => 'VideoListing updated successfully']);
    }

    /**
     * Video Listing Delete Method
     * 
     * <aside>Remove the specified Video Listing from storage.</aside>
     * @authenticated
     * 
     * @urlParam video_listing required The ID of the Video Listing.
     * 
     * @response {
     *  "message": "Video Listing deleted successfully"
     * }
     */
    public function destroy(VideoListing $videoListing)
    {
        $videoListing->delete();
        return response()->json(['message' => 'VideoListing deleted successfully']);
    }

    /**
     * Video Listing Share Link Method
     * 
     * <aside>Create o search the shareable link for the specified Video Listing's info in storage.</aside>
     * 
     * @authenticated
     * @urlParam id required The ID of the Video Listing.
     * 
     * 
     * @responseField message string
     * @responseField meetingURL Valid public URL of the video listing
     * 
     * @response scenario="Active Shareable Link doesn't exist" {
     *   "message": "Shareable Link for VideoListing was created successfully",
     *   "sharelink": "https:\\collok.io/shared/video_listing/{uuid}"
     * }
     * 
     * @response scenario="Active Shareable Link exist" {
     *   "sharelink": "https:\\collok.io/shared/video_listing/{uuid}"
     * }
     * 
     * @response scenario="Active Shareable Link exist and user send a new expire date" {
     *   "message": "Shareable Link for VideoListing was updated successfully",
     *   "sharelink": "https:\\collok.io/shared/video_listing/{uuid}"
     * }
     */
    public function makeShareableLink(StoreShareableLinkRequest $request, $id) {

        $user = Auth::user();

        $videoListing = VideoListing::findOrFail($id);
        $this->authorize('makeShareableLink', $videoListing);

        $shareable_link = $videoListing->shareableLinks()->where('active', 1)
        ->whereDate('expires_at', '>', now())->first();

        /**
         * Verify if the VideoListing has a existing active shareablelink
         * Otherwise, the system will created a new one
         */
        if ($shareable_link == null) {
            $expires_at = Carbon::now()->addDay();
            if(!is_null($request->get('expires_at'))){
                $expires_at = Carbon::createFromFormat('Y-m-d H:i:s', $request->get('expires_at'), 'GMT-5')->setTimezone('UTC'); 
            }
    
            $link = ShareableLink::buildFor($videoListing)->setActive()->setPrefix('video_listing')->notifyOnVisit()->setExpirationDate($expires_at)->build();
            return response()->json([
                'message' => 'Shareable Link for VideoListing was created successfully',
                'sharelink' => $link->url
            ]);
        } else {
            $responseJSON = [
                'sharelink' => $shareable_link->url
            ];

            // If the user send a expire date , the ShareableLink will be updated
            if(!is_null($request->get('expires_at'))){
                $expires_at = Carbon::createFromFormat('Y-m-d H:i:s', $request->get('expires_at'), 'GMT-5')->setTimezone('UTC');
                $shareable_link->setAttribute('expires_at', $expires_at);
                $shareable_link->save();

                $responseJSON['message'] = 'Shareable Link for VideoListing was updated successfully';
            }

            return response()->json($responseJSON);
        }

    }
}
