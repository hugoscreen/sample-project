<?php

namespace App\Http\Controllers;

use App\Application;
use App\Document;
use App\Http\Requests\StoreDocumentRequest;
use App\Http\Resources\DocumentCollection;
use Illuminate\Support\Facades\Storage;
use \App\Http\Resources\Document as DocumentResource;
use \App\Http\Resources\Application as ApplicationResource;

/**
 * @group Document Controller
 *
 * <aside>APIs for managing document model</aside>
 */
class DocumentController extends Controller
{

    /**
     * Document Create Method
     * 
     * <aside>Create a new Document entry in storage</aside>
     * @authenticated
     * 
     * @responseField data object Document Resource
     * 
     * @apiResourceCollection App\Http\Resources\DocumentCollection
     * @apiResourceModel App\Document
     */
    public function store(StoreDocumentRequest $request)
    {
        // get the application to associate the files
        $application = Application::findOrFail($request->get('application_id'));

        //iterate and save all the files
        foreach ($request->file('files') as $file){
            $name = date('Y-m-d-H-i-s-').uniqid();
            $document = new Document();
            $document->setAttribute('name',$application->applicant->user->id.'/'.$application->applicant->id.'/'.$application->id.'/'.$name.'.'.$file->getClientOriginalExtension());
            $document->setAttribute('description',$file->getClientOriginalName());
            $document->application()->associate($application);
            $document->save();
            $file->storeAs($application->applicant->user->id.'/'.$application->applicant->id.'/'.$application->id,
                $name.'.'.$file->getClientOriginalExtension(),
            'do');
        }


        return (new DocumentCollection($application->documents))->additional(['message' => 'Document(s) uploaded and assigned successfully']);

    }

    /**
     * Document Delete Method
     * 
     * <aside>Remove the specified Document from storage.</aside>
     * @authenticated
     * 
     * @urlParam document required The ID of the Document.
     * 
     * @response {
     *  "message": "Document deleted successfully"
     * }
     */
    public function destroy($id)
    {
        $document = Document::findOrFail($id);
        $application = $document->application;
        $document->delete();
        return (new DocumentCollection($application->documents))->additional(['message' => 'Document deleted successfully']);

    }

}
