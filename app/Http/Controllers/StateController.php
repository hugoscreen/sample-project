<?php

namespace App\Http\Controllers;

use App\Country;
use App\State;
use App\Http\Requests\StoreStateRequest;
use App\Http\Requests\UpdateStateRequest;
use App\Http\Resources\StateCollection;
use Illuminate\Http\Request;
use App\Http\Resources\State as StateResource;

/**
 * @group State Controller
 *
 * <aside>APIs for managing state model</aside>
 */
class StateController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(State::class);
    }

    /**
     * State Index Method
     * 
     * <aside>Get a list of all the State in storage.</aside>
     * @authenticated
     * 
     * @responseField data array State Collection Resource
     * 
     * @apiResourceCollection App\Http\Resources\StateCollection
     * @apiResourceModel App\State
     */
    public function index()
    {
        return new StateCollection(State::all());
    }

    /**
     * State Create Method
     * 
     * <aside>Create a new State entry in storage</aside>
     * @authenticated
     *
     * @responseField data object State Resource
     * 
     * @apiResource App\Http\Resources\State
     * @apiResourceModel App\State
     */
    public function store(StoreStateRequest $request)
    {
        $state = new State();
        $state->setAttribute('name', $request->get('name'));
        $country =  Country::findOrFail($request->get('country_id'));
        $state->country()->associate($country);
        $state->save();
        return (new StateResource($state))->additional(['message' => 'State created successfully']);
    }

    /**
     * State Show Method
     * <aside>Get info from the specified State in storage.</aside>
     * @authenticated
     *
     * @urlParam state required The ID of the State.
     * 
     * @responseField data object State Resource
     * 
     * @apiResource App\Http\Resources\State
     * @apiResourceModel App\State
     */
    public function show(State $state)
    {
        return new StateResource($state);
    }

    /**
     * State Update Method
     * 
     * <aside>Update the specified State's info in storage.</aside>
     * @authenticated
     * @urlParam state required The ID of the State.
     * 
     * @bodyParam name string
     * 
     * @responseField data object State Resource
     * 
     * @apiResource App\Http\Resources\State
     * @apiResourceModel App\State
     */
    public function update(UpdateStateRequest $request, State $state)
    {
        if(!is_null($request->get('name'))){
            $state->setAttribute('name', $request->get('name'));
        }

        if(!is_null($request->get('country_id'))){
            $country =  Country::findOrFail($request->get('country_id'));
            $state->country()->associate($country);
        }

        $state->save();

        return (new StateResource($state))->additional(['message' => 'State updated successfully']);
    }

    /**
     * State Delete Method
     * 
     * <aside>Remove the specified State from storage.</aside>
     * @authenticated
     * 
     * @urlParam state required The ID of the State.
     * 
     * @response {
     *  "message": "State deleted successfully"
     * }
     */
    public function destroy(State $state)
    {
        $state->delete();
        return response()->json([
            'message' => 'State deleted successfully'
        ]);
    }
}
