<?php

namespace App\Http\Controllers;

use App\Position;
use App\Questionset;
use App\Http\Requests\StoreQuestionsetRequest;
use App\Http\Requests\UpdateQuestionsetRequest;
use App\Http\Requests\DuplicateQuestionsetRequest;
use App\Http\Resources\QuestionsetCollection;
use Illuminate\Http\Request;
use App\Http\Resources\Questionset as QuestionsetResource;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/**
 * @group QuestionSet Controller
 *
 * <aside>APIs for managing question set model</aside>
 */
class QuestionsetController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Questionset::class);
    }

    /**
     * Questionset Index Method
     *
     * <aside>Get a list of all the Questionset in storage.</aside>
     * @authenticated
     *
     * @responseField data array Questionset Collection Resource
     *
     * @apiResourceCollection App\Http\Resources\QuestionsetCollection
     * @apiResourceModel App\Questionset
     */
    public function index()
    {
        return new QuestionsetCollection(Questionset::public()->get());
    }

    /**
     * Questionset Create Method
     *
     * <aside>Create a new Questionset entry in storage</aside>
     * @authenticated
     *
     * @responseField data object Questionset Resource
     *
     * @apiResource App\Http\Resources\Questionset
     * @apiResourceModel App\Questionset
     */
    public function store(StoreQuestionsetRequest $request)
    {
        $questionset = new Questionset();
        $questionset->setAttribute('name', $request->get('name'));
        $questionset->setAttribute('instructions', $request->get('instructions'));

        if(!is_null($request->get('is_public'))){
            $questionset->setAttribute('is_public', $request->get('is_public'));
        }

        $user = Auth::user();
        $questionset->user()->associate($user);

        $questionset->save();


        if (!is_null($request->get('positions'))) {
            $positions =  Position::findOrFail($request->get('positions'));
            $questionset->positions()->sync($positions);
        }

        foreach ($questionset->positions as $position){
            Storage::disk('do')->put(
                'position/'.$position->id.'/questionsets/'.$questionset->id.'.pdf',
                $this->pdf($questionset->id, $position->id),
                'public'
            );
        }

        return (new QuestionsetResource($questionset))->additional(['message' => 'Questionset created successfully']);
    }

    /**
     * Questionset Show Method
     * <aside>Get info from the specified Questionset in storage.</aside>
     * @authenticated
     *
     * @urlParam questionset required The ID of the Questionset.
     *
     * @responseField data object Questionset Resource
     *
     * @apiResource App\Http\Resources\Questionset
     * @apiResourceModel App\Questionset
     */
    public function show(Questionset $questionset)
    {
        $questionset->load(['questions', 'positions']);
        return new QuestionsetResource($questionset);
    }

    /**
     * Questionset Update Method
     *
     * <aside>Update the specified Questionset's info in storage.</aside>
     * @authenticated
     * @urlParam questionset required The ID of the Questionset.
     *
     * @bodyParam name string
     * @bodyParam instructions string
     *
     * @responseField data object Questionset Resource
     *
     * @apiResource App\Http\Resources\Questionset
     * @apiResourceModel App\Questionset
     */
    public function update(UpdateQuestionsetRequest $request, Questionset $questionset)
    {
        if(!is_null($request->get('name'))){
            $questionset->setAttribute('name', $request->get('name'));
        }

        if(!is_null($request->get('instructions'))){
            $questionset->setAttribute('instructions', $request->get('instructions'));
        }

        if(!is_null($request->get('is_public'))){
            $questionset->setAttribute('is_public', $request->get('is_public'));
        }

        if(!is_null($request->get('positions'))) {
            $positions =  Position::findOrFail($request->get('positions'));
            $questionset->positions()->sync($positions);
        }

        $questionset->save();

        foreach ($questionset->positions as $position){
            Storage::disk('do')->put(
                'position/'.$position->id.'/questionsets/'.$questionset->id.'.pdf',
                $this->pdf($questionset->id, $position->id),
                'public'
            );
        }

        return (new QuestionsetResource($questionset))->additional(['message' => 'Questionset updated successfully']);
    }

    /**
     * Questionset Delete Method
     *
     * <aside>Remove the specified Questionset from storage.</aside>
     * @authenticated
     *
     * @urlParam questionset required The ID of the Questionset.
     *
     * @response {
     *  "message": "Questionset deleted successfully"
     * }
     */
    public function destroy(Questionset $questionset)
    {
        $questionset->delete();
        return response()->json([
            'message' => 'Questionset deleted successfully'
        ]);
    }

    /**
     * Questionset Get PDF Method
     *
     * <aside>Get stream from the specified Questionset's PDF from storage.</aside>
     * @authenticated
     *
     * @urlParam id required The ID of the Questionset.
     *
     */
    public static function pdf($questionset_id, $position_id)
    {
        $questionset = Questionset::findOrFail($questionset_id);
        $position = Position::findOrFail($position_id);
        $data['questionset'] = $questionset;
        $data['positionName'] = $position->name;
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf.ondemand_questions', $data)->setPaper('letter', 'landscape');;
        return $pdf->stream();
    }

    /**
     * Questionset Duplication Method
     *
     * <aside>Duplicate the selected questionset and it associates to the position.</aside>
     * @authenticated
     *
     * @responseField data object Questionset Resource
     *
     * @apiResource App\Http\Resources\Questionset
     * @apiResourceModel App\Questionset
     */
    public function duplicates(DuplicateQuestionsetRequest $request)
    {
        $questionset =  Questionset::findOrFail($request->get('questionset_id'));
        $position =  Position::findOrFail($request->get('position_id'));
        $user = Auth::user();

        $dupQuestionset = $questionset->replicate();
        $dupQuestionset->user()->associate($user);
        $dupQuestionset->save();

        $dupQuestions = [];
        foreach ($questionset->questions as $question) {
            array_push($dupQuestions, $question->replicate());
        }

        $dupQuestionset->positions()->sync($position);
        $dupQuestionset->questions()->saveMany($dupQuestions);
        return (new QuestionsetResource($dupQuestionset->load(['questions'])))->additional(['message' => 'Questionset duplicated successfully']);
    }
}
