<?php

namespace App\Http\Controllers;

use App\User;
use App\Email;
use App\Company;
use App\Comment;
use App\Recruiter;
use App\Contractor;
use App\Notification;
use App\Liveinterview;
use App\Ondemandinterview;
use App\Events\NewNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Http\Resources\CommentCollection;
use App\Http\Resources\Comment as CommentResource;

/**
 * @group Comment Controller
 *
 * <aside>APIs for managing comment model</aside>
 */
class CommentController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Comment::class);
    }

    /**
     * Comment Index Method
     *
     * <aside>Get a list of all the Comments in storage.</aside>
     * @authenticated
     *
     * @responseField data array Comment Collection Resource
     *
     * @apiResourceCollection App\Http\Resources\CommentCollection
     * @apiResourceModel App\Comment
     */
    public function index()
    {
        return new CommentCollection(Comment::all());
    }

    /**
     * Comment Create Method
     *
     * <aside>Create a new Comment entry in storage</aside>
     * @authenticated
     *
     * @responseField data object Comment Resource
     *
     * @apiResource App\Http\Resources\Comment
     * @apiResourceModel App\Comment
     */
    public function store(StoreCommentRequest $request)
    {
        $comment = new Comment();

        $comment->setAttribute('comment', $request->get('comment'));
        $comment->setAttribute('type', $request->get('type'));

        if(!is_null($request->get('contractor_id'))){
            $contractor =  Contractor::findOrFail($request->get('contractor_id'));
            $comment->setAttribute('user_name', $contractor->user->name);
            $comment->setAttribute('user_role_position', $contractor->position);
            $comment->contractor()->associate($contractor);
        }

        if(!is_null($request->get('recruiter_id'))){
            $recruiter =  Recruiter::findOrFail($request->get('recruiter_id'));
            $comment->setAttribute('user_name', $recruiter->user->name);
            $comment->setAttribute('user_role_position', $recruiter->position);
            $comment->recruiter()->associate($recruiter);
        }

        if(empty($comment->getAttribute('user_name'))) {
            $user = Auth::user();
            $comment->setAttribute('user_name', $user->name);

            if($user->contractor()->exists()){
                $contractor =  Contractor::findOrFail($user->contractor->id);
                $comment->contractor()->associate($contractor);
                $comment->setAttribute('user_role_position', $user->contractor->position);
            }

            if($user->recruiter()->exists()){
                $recruiter =  Recruiter::findOrFail($user->recruiter->id);
                $comment->recruiter()->associate($recruiter);
                $comment->setAttribute('user_role_position', $user->recruiter->position);
            }
        }

        $application;
        if(!is_null($request->get('liveinterview_id'))){
            $liveinterview =  Liveinterview::findOrFail($request->get('liveinterview_id'));
            $application = $liveinterview->application;
            $comment->liveinterview()->associate($liveinterview);
        }

        if(!is_null($request->get('ondemandinterview_id'))){
            $ondemandinterview =  Ondemandinterview::findOrFail($request->get('ondemandinterview_id'));
            $application = $ondemandinterview->application;
            $comment->ondemandinterview()->associate($ondemandinterview);
        }
        $comment->save();

        $email = new Email();
        $email->template = 'new-evaluation-applicant';
        $email->user_id = $application->applicant->user_id;
        $email->subject = 'Collok.io: New Interview Review for '. $application->applicant->user->name;

        $email->data = [
            'name' => $application->applicant->user->name,
            'position' => $application->position->name
        ];
        $emailResponse = $email->sendMail();

        $notification = new Notification();
        $notification->setAttribute('title', 'New Evaluation in your Interview');
        $notification->setAttribute('text', 'You have recieved an evaluation in your interview from '.$application->position->name.' position.');
        $notification->setAttribute('type', 'info');
        $notification->user()->associate($application->applicant->user);
        $notification->save();
        broadcast(new NewNotification($notification));

        return (new CommentResource($comment))->additional(['message' => 'Comment created successfully']);
    }

    /**
     * Comment Show Method
     * <aside>Get info from the specified Comment in storage.</aside>
     * @authenticated
     *
     * @urlParam comment required The ID of the Comment.
     *
     * @responseField data object Comment Resource
     *
     * @apiResource App\Http\Resources\Comment
     * @apiResourceModel App\Comment
     */
    public function show(Comment $comment)
    {
        $comment->load('liveinterview','ondemandinterview');
        return new CommentResource($comment);
    }

    /**
     * Comment Update Method
     *
     * <aside>Create a new Comment entry in storage</aside>
     * @authenticated
     *
     * @responseField data object Comment Resource
     *
     * @apiResource App\Http\Resources\Comment
     * @apiResourceModel App\Comment
     */
    public function update(UpdateCommentRequest $request, Comment $comment)
    {
        if(!is_null($request->get('comment'))){
            $comment->setAttribute('comment', $request->get('comment'));
        }

        if(!is_null($request->get('type'))){
            $comment->setAttribute('type', $request->get('type'));
        }

        if(!is_null($request->get('contractor_id'))){
            $contractor =  Contractor::findOrFail($request->get('contractor_id'));
            $comment->contractor()->associate($contractor);
        }

        if(!is_null($request->get('recruiter_id'))){
            $recruiter =  Recruiter::findOrFail($request->get('recruiter_id'));
            $comment->recruiter()->associate($recruiter);
        }

        if(!is_null($request->get('liveinterview_id'))){
            $liveinterview =  Liveinterview::findOrFail($request->get('liveinterview_id'));
            $comment->liveinterview()->associate($liveinterview);
        }

        if(!is_null($request->get('ondemandinterview_id'))){
            $ondemandinterview =  Ondemandinterview::findOrFail($request->get('ondemandinterview_id'));
            $comment->ondemandinterview()->associate($ondemandinterview);
        }

        $comment->save();

        return (new CommentResource($comment))->additional(['message' => 'Comment updated successfully']);
    }

    /**
     * Comment Delete Method
     *
     * <aside>Remove the specified Comment from storage.</aside>
     * @authenticated
     *
     * @urlParam comment required The ID of the Comment.
     *
     * @response {
     *  "message": "Comment deleted successfully"
     * }
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        return response()->json([
            'message' => 'Comment deleted successfully'
        ]);
    }

    /**
     * Comments LiveIntervew/OndemandInterview Method
     *
     * <aside>Get a list of all the Comments in storage.</aside>
     * @authenticated
     *
     * @urlParam type required The value must be one of live or on_demand.
     * @urlParam id required The ID of the Liveinterview or Ondemandinterview.
     *
     * @responseField data array Comment Collection Resource
     *
     * @apiResourceCollection App\Http\Resources\CommentCollection
     * @apiResourceModel App\Comment
     */
    public function getInterviewComments($type, $id)
    {
        if ($type == 'live'){
            $meeting =  Liveinterview::findOrFail($id);
            return (new CommentCollection($meeting->comments));
        }

        if ($type == 'on_demand'){
            $meeting =  Ondemandinterview::findOrFail($id);
            return (new CommentCollection($meeting->comments));
        }

        return abort(500,'Something went wrong please contact the administrator');

    }
}
