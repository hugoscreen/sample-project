<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\StoreCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;
use App\Http\Resources\CompanyCollection;
use App\State;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\Company as CompanyResource;
use Illuminate\Support\Facades\Auth;

/**
 * @group Company Controller
 *
 * <aside>APIs for managing company model</aside>
 */
class CompanyController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Company::class);
    }

    /**
     * Company Index Method
     * 
     * <aside>Get a list of all the Companies in storage.in base of user's role.
     * A contractor user can retrieve only the information of its company,
     * otherwise will retrieve all companies in the database.</aside>
     *
     * @authenticated
     * @responseField data array Company Collection Resource
     * 
     * @apiResourceCollection App\Http\Resources\CompanyCollection
     * @apiResourceModel App\Company
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('contractor')) {
            return new CompanyCollection(
                Company::where('id','=',$user->contractor->company_id)
                ->with('positions')
                ->get()
            );
        } else {
            return new CompanyCollection(Company::with('positions')->get());
        }
    }

    /**
     * Company Create Method
     * 
     * <aside>Create a new Company entry in storage</aside>
     * @authenticated
     * 
     * @bodyParam profile_picture image The profile picture for the user.
     * 
     * @responseField data object Company Resource
     * 
     * @apiResource App\Http\Resources\Company
     * @apiResourceModel App\Company
     */
    public function store(StoreCompanyRequest $request)
    {
        //TODO: add logo field
        $company = new Company();
        $company->setAttribute('name', $request->get('name'));
        $company->setAttribute('phone', $request->get('phone'));
        $state =  State::findOrFail($request->get('state_id'));
        $company->state()->associate($state);
        $company->setAttribute('city', $request->get('city'));
        $company->setAttribute('video', $request->get('video'));
        $company->save();

        if ($request->hasFile('profile_picture')) {
            $file = $request->file('profile_picture');
            $name = date('Y-m-d-H-i-s-').uniqid();
            $company->setAttribute('profile_picture', 'companies/'.$company->id.'/'.$name.'.'.$file->getClientOriginalExtension());
            $file->storeAs('companies/'.$company->id, $name.'.'.$file->getClientOriginalExtension(), 'do');
            $company->save();
        }
        return (new CompanyResource($company))->additional(['message' => 'Company created successfully']);
    }

    /**
     * Company Show Method
     * <aside>Get info from the specified Company in storage.</aside>
     * @authenticated
     *
     * @urlParam company required The ID of the Company.
     * 
     * @responseField data object Company Resource
     * 
     * @apiResource App\Http\Resources\Company
     * @apiResourceModel App\Company
     */
    public function show(Company $company)
    {
        $company->load('positions');
        return new CompanyResource($company);
    }

    /**
     * Company Update Method
     * 
     * <aside>Update the specified Company's info in storage. To update or upload a profile picture (file),
     * the request should change to POST and add a extra bodyparam called "_method" with value "PUT"</aside>
     * 
     * @authenticated
     * @urlParam Company required The ID of the Company.
     * 
     * @bodyParam profile_picture image The profile picture for the user.
     * 
     * @responseField data object Company Resource
     * 
     * @apiResource App\Http\Resources\Company
     * @apiResourceModel App\Company
     */
    public function update(UpdateCompanyRequest $request, Company $company)
    {
        if(!is_null($request->get('name'))){
            $company->setAttribute('name', $request->get('name'));
        }

        if(!is_null($request->get('phone'))){
            $company->setAttribute('phone', $request->get('phone'));
        }

        if(!is_null($request->get('state_id'))){
            $user =  State::findOrFail($request->get('state_id'));
            $company->state()->associate($user);
        }

        if(!is_null($request->get('city'))){
            $company->setAttribute('city', $request->get('city'));
        }

        if(!is_null($request->get('video'))){
            $company->setAttribute('video', $request->get('video'));
        }

        if ($request->hasFile('profile_picture')) {
            if (!empty($company->getAttribute('profile_picture'))) {
                if (Storage::disk('do')->exists($company->getAttribute('profile_picture'))) {
                    Storage::disk('do')->delete($company->getAttribute('profile_picture'));
                }
            }
            $file = $request->file('profile_picture');
            $name = date('Y-m-d-H-i-s-').uniqid();
            $company->setAttribute('profile_picture', 'companies/'.$company->id.'/'.$name.'.'.$file->getClientOriginalExtension());
            $file->storeAs('companies/'.$company->id, $name.'.'.$file->getClientOriginalExtension(), 'do');
        }
        $company->save();

        return (new CompanyResource($company))->additional(['message' => 'Company updated successfully']);
    }

    /**
     * Company Delete Method
     * 
     * <aside>Remove the specified Company from storage.</aside>
     * @authenticated
     * 
     * @urlParam company required The ID of the Company.
     * 
     * @response {
     *  "message": "Company deleted successfully"
     * }
     */
    public function destroy(Company $company)
    {
        $company->delete();
        return response()->json([
            'message' => 'Company deleted successfully'
        ]);
    }
}
