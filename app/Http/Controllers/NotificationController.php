<?php

namespace App\Http\Controllers;

use App\Notification;
use App\Events\NewNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreNotificationRequest;
use App\Http\Requests\UpdateNotificationRequest;
use App\Http\Resources\NotificationCollection;
use App\Http\Resources\Notification as NotificationResource;

/**
 * @group Notification Controller
 *
 * <aside>APIs for managing Notification model</aside>
 */
class NotificationController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Notification::class);
    }

    /**
     * Notification Index Method
     *
     * <aside>Get a list of all the Notification in storage.</aside>
     * @authenticated
     *
     * @responseField data array Notification Collection Resource
     *
     * @apiResourceCollection App\Http\Resources\NotificationCollection
     * @apiResourceModel App\Notification
     */
    public function index()
    {
        $user = Auth::user();
        $notifications = Notification::where('user_id', $user->id)->get();
        return new NotificationCollection($notifications);
    }

    /**
     * Notification Create Method
     *
     * <aside>Create a new Notification entry in storage</aside>
     * @authenticated
     *
     * @responseField data object Notification Resource
     *
     * @apiResource App\Http\Resources\Notification
     * @apiResourceModel App\Notification
     */
    public function store(StoreNotificationRequest $request)
    {
        $notification = new Notification();
        $notification->setAttribute('title', $request->get('title'));
        $notification->setAttribute('text', $request->get('text'));
        $notification->setAttribute('type', $request->get('type'));

        $user = Auth::user();
        $notification->user()->associate($user);
        $notification->save();
        broadcast(new NewNotification($notification));

        return new NotificationResource($notification);
    }

    /**
     * Notification Show Method
     *
     * <aside>Get info from the specified Notification in storage.</aside>
     * @authenticated
     *
     * @urlParam notification required The ID of the Notification.
     *
     * @responseField data object Notification Resource
     *
     * @apiResource App\Http\Resources\Notification
     * @apiResourceModel App\Notification
     */
    public function show(Notification $notification)
    {
        return new NotificationResource($notification);
    }

    /**
     * Notification Update Method
     *
     * <aside>Update the specified Notification's info in storage.</aside>
     *
     * @authenticated
     * @urlParam notification required The ID of the Notification.
     *
     * @responseField data object Notification Resource
     *
     * @apiResource App\Http\Resources\Notification
     * @apiResourceModel App\Notification
     */
    public function update(UpdateNotificationRequest $request, Notification $notification)
    {
        $notification->setAttribute('read', $request->get('read'));
        $notification->save();
        return new NotificationResource($notification);
    }

    /**
     * Notification Delete Method
     *
     * <aside>Remove the specified Notification from storage.</aside>
     * @authenticated
     *
     * @urlParam notification required The ID of the Notification.
     *
     * @response {
     *  "message": "Notification deleted successfully"
     * }
     */
    public function destroy(Notification $notification)
    {
        $notification->delete();
        return response()->json([
            'message' => 'Notification deleted successfully'
        ]);
    }


    /**
     * Notification Mark All Read
     *
     * <aside>Mark all the user's notifications as read.</aside>
     * @authenticated
     *
     * @response {
     *  "message": "All notifications have been marked as read."
     * }
     */
    public function markAllRead()
    {
        Notification::where('user_id', Auth::user()->id)->update(['read' => true]);
        return response()->json([
            'message' => 'All notifications have been marked as read.'
        ]);
    }
}
