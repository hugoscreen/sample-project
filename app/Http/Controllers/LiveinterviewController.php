<?php

namespace App\Http\Controllers;

use App\BBB;
use App\Email;
use Carbon\Carbon;
use App\Recruiter;
use App\Contractor;
use App\Application;
use App\Notification;
use App\Liveinterview;
use App\Events\NewNotification;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\CommentCollection;
use App\Http\Requests\StoreLiveinterviewRequest;
use App\Http\Requests\UpdateLiveinterviewRequest;
use App\Http\Resources\LiveInterviewCommentCollection;
use App\Http\Resources\Liveinterview as LiveinterviewResource;

/**
 * @group LiveInterview Controller
 *
 * <aside>APIs for managing liveinterview model</aside>
 */
class LiveinterviewController extends Controller
{

    /**
     * Liveinterview Create Method
     *
     * <aside>Create a new Liveinterview entry in storage and send a mail
     * notification about the about the liveinterview created</aside>
     * @authenticated
     *
     * @responseField data object Liveinterview Resource
     *
     * @apiResource App\Http\Resources\Liveinterview
     * @apiResourceModel App\Liveinterview
     */
    public function store(StoreLiveinterviewRequest $request)
    {
        $validData = $request->validated();
        $recruiter = Recruiter::findOrFail($validData['recruiter_id']);
        $contractor = Contractor::findOrFail($validData['contractor_id']);
        $application = Application::findOrFail($validData['application_id']);
        $position = $application->getAttribute('position');

        $liveInterview = new Liveinterview();
        $liveInterview->recruiter()->associate($recruiter);
        $liveInterview->application()->associate($application);
        $liveInterview->contractor()->associate($contractor);
        $liveInterview->setAttribute('scheduled', $validData['scheduled']);
        if (array_key_exists('show_hiring_manager', $validData) && !is_null($validData['show_hiring_manager'])) {
            $liveInterview->setAttribute('show_hiring_manager', $validData['show_hiring_manager']);
        }
        $liveInterview->save();
        $liveInterview->setAttribute('internalMeetingId', $liveInterview->getAttribute('id').uniqid("", true));
        $BBB = new BBB();
        $liveInterview->setAttribute('BBBInternalId',
            $BBB->createRoom($liveInterview->getAttribute('internalMeetingId'), $position));
        $liveInterview->save();

        // generating notification
        $email = new Email();
        $email->user_id = $application->applicant->user_id;
        $email->url = env('FRONT_URL', 'https://collok.io')."/login?email=".$application->applicant->user->email;
        $email->subject = 'Collok.io - you have a live interview for '.$application->position->name.' position';
        $email->message = 'Hello, Applicant '. $application->applicant->user->name.':<br/><br/>';
        $email->message .= 'You have received an email from the Collok.io platform.<br/><br/>';
        $email->message .= 'You have a new live interview scheduled.<br/>';
        $email->message .= 'Scheduled for: '. date('F d, Y - H:i',strtotime($validData['scheduled'])).' Hrs.<br/>';
        $email->message .= 'Company: '.$application->position->company->name.'<br/>';
        $email->message .= 'Position: '.$application->position->name. '<br/>';
        $email->sendMail(false);

        $notification = new Notification();
        $notification->setAttribute('title', 'A new live interview');
        $notification->setAttribute('text', 'You have a new live interview scheduled.');
        $notification->setAttribute('type', 'info');
        $notification->user()->associate($application->applicant->user);
        $notification->save();
        broadcast(new NewNotification($notification));

        return response()->json(['message' => 'created successfully', 'data' => $liveInterview]);
    }

    /**
     * Liveinterview Join Method
     *
     * <aside>Join to the specified Liveinterview.</aside>
     * @authenticated
     *
     * @urlParam live_interview required The internalMeetingId of the Liveinterview.
     *
     * @responseField message string
     * @responseField meetingURL Valid URL of the live interview or waiting for live interview
     *
     * @response {
     *  "message": "Successful url fetch for live join",
     *  "meetingURL": "validURL"
     * }
     *
     * @response scenario="The interview hasn't started yet" {
     *  "message": "The interview hasn't started yet",
     *  "meetingURL": "https://collok.io/waitingForInterview"
     * }
     */
    public function show($meeting_id)
    {
        $user = Auth::user();
        $BBB = new BBB();

        $liveInterview = Liveinterview::where('internalMeetingId', $meeting_id)->first();

        // Set datetime in timezonte GMT-5
        $dateScheduled = Carbon::createFromFormat('Y-m-d H:i:s', $liveInterview->getAttribute('scheduled'), 'GMT-5');
        // Get the actual datetime in timezone GMT-5
        $dateToday = now()->setTimezone('GMT-5');
        // Check if the Interview has already started
        if ($dateToday >= $dateScheduled) {
            if (is_null($liveInterview->BBBinternalId)) {
                $application = Application::findOrFail($liveInterview->application_id);
                $position = $application->getAttribute('position');

                $liveInterview->setAttribute('BBBInternalId',
                    $BBB->createRoom($liveInterview->getAttribute('internalMeetingId'), $position));
                $liveInterview->save();
            }

            $response = $BBB->joinMeeting($meeting_id, $user);
            return response()->json($response);
        } else {
            //If the liveInterview hasn't started yet, the system redirect it to other page.
            return response()->json(['message'=>'The interview hasn\'t started yet.', 'meetingURL'=>env('FRONT_URL', 'https://collok.io').'/waitingForInterview']);
        }


    }

    /**
     * Liveinterview Update Method
     *
     * <aside>Update the specified Liveinterview's info in storage.</aside>
     * @authenticated
     * @urlParam live_interview required The ID of the Liveinterview.
     *
     * @responseField data object Liveinterview Resource
     *
     * @apiResource App\Http\Resources\Liveinterview
     * @apiResourceModel App\Liveinterview
     */
    public function update(UpdateLiveinterviewRequest $request, Liveinterview $liveInterview)
    {
        if(!is_null($request->get('scheduled'))){
            $liveInterview->setAttribute('scheduled', $request->get('scheduled'));
        }

        if(!is_null($request->get('status'))){
            $liveInterview->setAttribute('status', $request->get('status'));
        }

        if(!is_null($request->get('show_hiring_manager'))){
            $liveInterview->setAttribute('show_hiring_manager', $request->get('show_hiring_manager'));
        }

        $liveInterview->save();

        return (new LiveinterviewResource($liveInterview))->additional(['message' => 'LiveInterview updated successfully']);
    }

}
