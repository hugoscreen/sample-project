<?php

namespace App\Http\Controllers;

use App\Language;
use App\Http\Requests\StoreLanguageRequest;
use App\Http\Requests\UpdateLanguageRequest;
use App\Http\Resources\LanguageCollection;
use App\State;
use Illuminate\Http\Request;
use App\Http\Resources\Language as LanguageResource;
use Illuminate\Support\Facades\Hash;

/**
 * @group Language Controller
 *
 * <aside>APIs for managing language model</aside>
 */

class LanguageController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Language::class);
    }

    /**
     * Language Index Method
     * 
     * <aside>Get a list of all the Language in storage.</aside>
     * @authenticated
     * 
     * @responseField data array Language Collection Resource
     * 
     * @apiResourceCollection App\Http\Resources\LanguageCollection
     * @apiResourceModel App\Language
     */
    public function index()
    {
        return new LanguageCollection(Language::all());
    }

    /**
     * Language Create Method
     * 
     * <aside>Create a new Language entry in storage</aside>
     * @authenticated
     *
     * @responseField data object Language Resource
     * 
     * @apiResource App\Http\Resources\Language
     * @apiResourceModel App\Language
     */
    public function store(StoreLanguageRequest $request)
    {
        $language = new Language();
        $language->setAttribute('name', $request->get('name'));
        $language->save();
        return (new LanguageResource($language))->additional(['message' => 'Language created successfully']);
    }

    /**
     * Language Show Method
     * <aside>Get info from the specified Language in storage.</aside>
     * @authenticated
     *
     * @urlParam language required The ID of the Language.
     * 
     * @responseField data object Language Resource
     * 
     * @apiResource App\Http\Resources\Language
     * @apiResourceModel App\Language
     */
    public function show(Language $language)
    {
        return new LanguageResource($language);
    }

    /**
     * Language Update Method
     * 
     * <aside>Update the specified Language's info in storage.</aside>
     * @authenticated
     * @urlParam language required The ID of the Language.
     * 
     * @bodyParam name string
     * 
     * @responseField data object Language Resource
     * 
     * @apiResource App\Http\Resources\Language
     * @apiResourceModel App\Language
     */
    public function update(UpdateLanguageRequest $request, Language $language)
    {
        if(!is_null($request->get('name'))){
            $language->setAttribute('name', $request->get('name'));
        }

        $language->save();

        return (new LanguageResource($language))->additional(['message' => 'Language updated successfully']);
    }

    /**
     * Language Delete Method
     * 
     * <aside>Remove the specified Language from storage.</aside>
     * @authenticated
     * 
     * @urlParam language required The ID of the Language.
     * 
     * @response {
     *  "message": "Language deleted successfully"
     * }
     */
    public function destroy(Language $language)
    {
        $language->delete();
        return response()->json([
            'message' => 'Language deleted successfully'
        ]);
    }
}
