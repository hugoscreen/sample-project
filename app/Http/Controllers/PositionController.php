<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Application;
use App\BBB;
use App\Company;
use App\Email;
use App\Events\NewNotification;
use App\Http\Requests\ApplyPositionRequest;
use App\Language;
use App\Ondemandinterview;
use App\Position;
use App\Http\Requests\StorePositionRequest;
use App\Http\Requests\UpdatePositionRequest;
use App\Http\Resources\PositionCollection;
use App\Questionset;
use App\Skill;
use App\State;
use App\User;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Resources\Position as PositionResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @group Position Controller
 *
 * <aside>APIs for managing position model</aside>
 */
class PositionController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Position::class);
    }

    /**
     * Position Index Method
     *
     * <aside>Get a list of all the Position in storage.</aside>
     * @authenticated
     *
     * @responseField data array Position Collection Resource
     *
     * @apiResourceCollection App\Http\Resources\PositionCollection
     * @apiResourceModel App\Position
     */
    public function index()
    {
        return new PositionCollection(Position::all());
    }

    /**
     * Position Create Method
     *
     * <aside>Create a new Position entry in storage</aside>
     * @authenticated
     *
     * @bodyParam description string
     *
     * @responseField data object Position Resource
     *
     * @apiResource App\Http\Resources\Position
     * @apiResourceModel App\Position
     */
    public function store(StorePositionRequest $request)
    {
        $position = new Position();
        $position->setAttribute('name', $request->get('name'));

        if(!is_null($request->get('description'))){
            $position->setAttribute('description', $request->get('description'));
        }

        $company =  Company::findOrFail($request->get('company_id'));
        $position->company()->associate($company);

        $position->save();

        if(!is_null($request->get('languages'))){
            $languageSyncData = [];
            foreach($request->get('languages') as $language) {
                $languageSyncData[$language['id']] = ['level' => $language['level']];
            }
            $position->languages()->sync($languageSyncData);
        }

        if(!is_null($request->get('skills'))){
            $skillSyncData =  [];
            foreach($request->get('skills') as $skillData) {
                $skill = Skill::where('name','LIKE', $skillData['skill'])->first();
                if (!$skill) {
                    $skill = new Skill();
                    $skill->setAttribute('name', $skillData['skill']);
                    $skill->save();
                }
                $skillSyncData[$skill->id] = ['level' => $skillData['level']];
            }
            $position->skills()->sync($skillSyncData);
        }

        $position->load(['applications', 'skills', 'languages', 'questionsets', 'defaultQuestionSet']);
        return (new PositionResource($position))->additional(['message' => 'Position created successfully']);
    }

    /**
     * Position Show Method
     * <aside>Get info from the specified Position in storage.</aside>
     * @authenticated
     *
     * @bodyParam description string
     *
     * @urlParam position required The ID of the Position.
     *
     * @responseField data object Position Resource
     *
     * @apiResource App\Http\Resources\Position
     * @apiResourceModel App\Position
     */
    public function show(Position $position)
    {
        $position->load(['applications', 'skills', 'languages', 'questionsets', 'defaultQuestionSet']);
        return new PositionResource($position);
    }

    /**
     * Position Update Method
     *
     * <aside>Update the specified Position's info in storage.</aside>
     * @authenticated
     * @urlParam position required The ID of the Position.
     *
     * @bodyParam name string
     * @bodyParam description string
     *
     * @responseField data object Position Resource
     *
     * @apiResource App\Http\Resources\Position
     * @apiResourceModel App\Position
     */
    public function update(UpdatePositionRequest $request, Position $position)
    {
        if(!is_null($request->get('name'))){
            $position->setAttribute('name', $request->get('name'));
        }

        if(!is_null($request->get('description'))){
            $position->setAttribute('description', $request->get('description'));
        }

        if(!is_null($request->get('company_id'))){
            $company =  Company::findOrFail($request->get('company_id'));
            $position->company()->associate($company);
        }

        if(!is_null($request->get('languages'))){
            $languageSyncData = [];
            foreach($request->get('languages') as $language) {
                $languageSyncData[$language['id']] = ['level' => $language['level']];
            }
            $position->languages()->sync($languageSyncData);
        }

        if(!is_null($request->get('skills'))){
            $skillSyncData =  [];
            foreach($request->get('skills') as $skillData) {
                $skill = Skill::where('name','LIKE', $skillData['skill'])->first();
                if (!$skill) {
                    $skill = new Skill();
                    $skill->setAttribute('name', $skillData['skill']);
                    $skill->save();
                }
                $skillSyncData[$skill->id] = ['level' => $skillData['level']];
            }
            $position->skills()->sync($skillSyncData);
        }

        if (!is_null($request->get('questionsets'))) {
            $questionsets = Questionset::findOrFail($request->get('questionsets'));
            $position->questionsets()->sync($questionsets);
        }

        if (!is_null($request->get('default_questionset_id'))) {
            //Check if the default Question set exist inside the related ones of the position
            if ($position->questionsets->contains(Questionset::findOrFail($request->get('default_questionset_id')))) {
                $questionset = Questionset::findOrFail($request->get('default_questionset_id'));
                $position->defaultQuestionSet()->associate($questionset);
            }
        }

        $position->save();
        $position->load(['applications', 'skills', 'languages', 'questionsets', 'defaultQuestionSet']);
        return (new PositionResource($position))->additional(['message' => 'Position updated successfully']);
    }

    /**
     * Position Delete Method
     *
     * <aside>Remove the specified Position from storage.</aside>
     * @authenticated
     *
     * @urlParam position required The ID of the Position.
     *
     * @response {
     *  "message": "Position deleted successfully"
     * }
     */
    public function destroy(Position $position)
    {
        $position->delete();
        return response()->json([
            'message' => 'Position deleted successfully'
        ]);
    }

    /**
     * Position Apply Method
     *
     * <aside>User apply for the specified position, this method
     * creates a user with applicant role, an entre for ondemandinterview
     * and the system send a mail notification.</aside>
     * @authenticated
     *
     * @urlParam id required The ID of the Position. Example: 1
     *
     * @responseField message string
     *
     * @response {
     *  "message": "Automatic Applying successful generated"
     * }
     */
    public function apply(ApplyPositionRequest $request, $position_id){
        $position =  Position::findOrFail($position_id);
        $user;
        $applicant;
        if (is_null($request->get('name'))) {
            $user = User::where('email', $request->get('email'))->firstOrFail();
            if ($user->hasRole('applicant')) {
                $applicant = $user->applicant;
            } else {
                abort(422, 'The user doesn\'t have the applicant role.');
            }
        } else {
            //register new user
            $user = UserController::createUser($request);
            $applicant = new Applicant();

            //register new user as  applicant
            $user->syncRoles(['applicant']);
            $applicant->user()->associate($user);
            $applicant->save();

            $email = new Email();
            $email->template = 'new-aplicant';
            $email->user_id = $user->id;
            $email->subject = 'Welcome to Collok.io';

            $email->data = [
                'name' => $user->name,
                'mail' => $user->email,
                'password' => $user->unhashPassword
            ];

            $email->sendMail();
        }

        // Create new application
        $application = new Application();
        $application->setAttribute('status', 'new');
        $application->applicant()->associate($applicant);
        $application->position()->associate($position);
        $application->save();

        //Generate ondemands
        foreach ($position->questionsets as $questionset) {
            $ondemandInterview = new Ondemandinterview();
            $ondemandInterview->application()->associate($application);
            $ondemandInterview->questionset()->associate($questionset);
            $ondemandInterview->save();
            $ondemandInterview->setAttribute('internalMeetingId', $ondemandInterview->getAttribute('id').uniqid("", true));
            $BBB = new BBB();
            $ondemandInterview->setAttribute('BBBInternalId',
                $BBB->createRoom($ondemandInterview->getAttribute('internalMeetingId'), $position, $questionset->id, true));
            $ondemandInterview->save();
        }

        $email = new Email();
        $email->template = 'new-ondemand-applicant';
        $email->user_id = $user->id;
        $email->subject = 'New On Demand Interview';

        $email->data = [
            'name' => $user->name,
            'limit_date'=>date('Y-m-d H:i:s', strtotime('+7 days', time())),
            'company' => $position->company->name,
            'position' => $position->name
        ];

        $email->sendMail();

        $notificationApplicant = new Notification();
        $notificationApplicant->setAttribute('title', 'A New On Demand Interview');
        $notificationApplicant->setAttribute('text', 'You have a new on demand interview.');
        $notificationApplicant->setAttribute('type', 'info');
        $notificationApplicant->user()->associate($user);
        $notificationApplicant->save();
        broadcast(new NewNotification($notificationApplicant));

        foreach ($position->subscribers as $subscriber) {
            $notification = new Notification();
            $notification->setAttribute('title', 'On Demand Interview Recording');
            $notification->setAttribute('text', 'Applicant'.$user->name
                .' has enter to on demand interview'.$position->name
                .' position. The recording will be available soon.');
            $notification->setAttribute('type', 'info');
            $notification->user()->associate($subscriber);
            $notification->save();
            broadcast(new NewNotification($notification));
        }

        return response()->json([
            'message' => 'Automatic Applying successful generated'
        ]);
    }
}
