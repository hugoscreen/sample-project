<?php

namespace App\Http\Controllers;

use App\Email;
use App\Company;
use App\Document;
use App\Position;
use App\Applicant;
use App\Application;
use App\Notification;
use App\Events\NewNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreApplicationRequest;
use App\Http\Requests\UpdateApplicationRequest;
use App\Http\Resources\DocumentCollection;
use App\Http\Resources\ApplicationCollection;
use App\Http\Resources\Application as ApplicationResource;

/**
 * @group Application Controller
 *
 * <aside>APIs for managing application model</aside>
 */
class ApplicationController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Application::class);
    }

    /**
     * Application Index Method
     *
     * <aside>Get a list of all the applicantion in storage.</aside>
     * @authenticated
     *
     * @responseField data array Application Collection Resource
     *
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('applicant')) {
            $applicant = $user->applicant;
            $applications = $applicant->applications;
            $applications->load('position.company', 'applicant');
            return new ApplicationCollection($applications);
        } else {
            return new ApplicationCollection(Application::with(['position.company','applicant'])->get());
        }
    }

    /**
     * Application Create Method
     *
     * <aside>Create a new applicantion entry in storage</aside>
     * @authenticated
     *
     * @bodyParam status string The status of the application.
     * @bodyParam applicant_id integer The ID of an Applicant.
     * @bodyParam position_id integer The ID of an Position.
     *
     * @responseField data object Application Resource
     */
    public function store(StoreApplicationRequest $request)
    {
        $application = new Application();
        $application->setAttribute('status', $request->get('status'));

        $applicant =  Applicant::findOrFail($request->get('applicant_id'));
        $application->applicant()->associate($applicant);

        $position =  Position::findOrFail($request->get('position_id'));
        $application->position()->associate($position);

        $application->save();

        //generating notification
        $emailApplicant = new Email();
        $emailApplicant->template = 'new-ondemand-applicant';
        $emailApplicant->user_id = $application->applicant->user_id;
        $emailApplicant->subject = 'Collok.io - You are applying for '. $position->name.' position.';

        $emailApplicant->data = [
            'name' => $application->applicant->user->name,
            'company' => $position->company->name,
            'position' => $position->name
        ];
        $emailApplicant->sendMail();

        $emailRecruiter = new Email();
        $emailRecruiter->template = 'new-register-recruiter';
        $emailRecruiter->subject = 'Collok.io - A new Applicant applied to '. $position->name.' position.';
        $emailRecruiter->recipients = $application->position->subscribers;
        $emailRecruiter->data = [
            'position' => $position->name
        ];

        $emailRecruiter->sendMultipleMails();

        foreach ($application->position->subscribers as $user) {
            $notification = new Notification();
            $notification->setAttribute('title', 'A new applicant in the position');
            $notification->setAttribute('text', 'The applicant '.$applicant->user->name.' has applied to '.$position->name.' position.');
            $notification->setAttribute('type', 'info');
            $notification->user()->associate($user);
            $notification->save();
            broadcast(new NewNotification($notification));
        }
        return (new ApplicationResource($application))->additional(['message' => 'Application created successfully']);
    }

    /**
     * Application Show Method
     * <aside>Get info from the specified applicantion in storage.</aside>
     * @authenticated
     *
     * @urlParam applicantion required The ID of the Application.
     *
     * @responseField data object AppliApplicationcant Resource
     *
     */
    public function show(Application $application)
    {
        $application->load(['position', 'applicant', 'skills', 'languages']);
        return new ApplicationResource($application);
    }

    /**
     * Application Update Method
     *
     * <aside>Update the specified applicantion's info in storage.</aside>
     * @authenticated
     *
     * @bodyParam status string The status of the application.
     * @bodyParam applicant_id integer The ID of an Applicant.
     * @bodyParam position_id integer The ID of an Position.
     *
     * @responseField data object Application Resource
     */
    public function update(UpdateApplicationRequest $request, Application $application)
    {
        if(!is_null($request->get('status'))){
            $application->setAttribute('status', $request->get('status'));
        }

        if(!is_null($request->get('applicant_id'))){
            $applicant =  Applicant::findOrFail($request->get('applicant_id'));
            $application->applicant()->associate($applicant);
        }

        if(!is_null($request->get('position_id'))){
            $position =  Position::findOrFail($request->get('position_id'));
            $application->position()->associate($position);
        }

        $application->save();

        return (new ApplicationResource($application))->additional(['message' => 'Application updated successfully']);
    }

    /**
     * Application Delete Method
     *
     * <aside>Remove the specified applicantion from storage.</aside>
     * @authenticated
     *
     * @urlParam application required The ID of the Application.
     *
     * @response {
     *  "message": "Application deleted successfully"
     * }
     */
    public function destroy(Application $application)
    {
        $application->delete();
        return response()->json([
            'message' => 'Application deleted successfully'
        ]);
    }


    /**
     * Application's Docs Method
     *
     * <aside>Get the specified applicantion's documents from storage.</aside>
     * @authenticated
     *
     * @urlParam id required The ID of the Applicantion.
     *
     * @responseField data object Document Collection Resource
     *
     * @apiResourceCollection App\Http\Resources\DocumentCollection
     * @apiResourceModel App\Document
     */
    public function getDocuments($id)
    {
        $application = Application::findOrFail($id);
        return new DocumentCollection($application->documents);
    }
}
