<?php

namespace App\Http\Controllers;

use App\Question;
use App\Questionset;
use App\Http\Resources\QuestionCollection;
use App\Http\Resources\Question as QuestionResource;
use App\Http\Requests\StoreQuestionRequest;
use App\Http\Requests\UpdateQuestionRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * @group Question Controller
 *
 * <aside>APIs for managing question model</aside>
 */
class QuestionController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Question::class);
    }

    /**
     * Question Index Method
     *
     * <aside>Get a list of all the Question in storage.</aside>
     * @authenticated
     *
     * @responseField data array Question Collection Resource
     *
     * @apiResourceCollection App\Http\Resources\QuestionCollection
     * @apiResourceModel App\Question
     */
    public function index()
    {
        return new QuestionCollection(Question::with('questionset')->get());
    }

    /**
     * Question Create Method
     *
     * <aside>Create a new Question entry in storage</aside>
     * @authenticated
     *
     * @bodyParam extra_info string
     * @bodyParam questions.*.extra_info string
     *
     * @responseField data object Question Resource
     *
     * @apiResource App\Http\Resources\Question
     * @apiResourceModel App\Question
     */
    public function store(StoreQuestionRequest $request)
    {
        $questionset = null;
        $questions = [];
        $questionsResponse = [];
        if(!$request->get('questions')){
            $questionData = [];
            $questionData['question'] = $request->get('question');
            $questionData['extra_info'] = $request->get('extra_info');
            $questionData['order'] = $request->get('order');
            $questionData['questionset_id'] = $request->get('questionset_id');
            $questions[] = $questionData;
        } else {
            $questions = $request->get('questions');
        }

        foreach($questions as $questionData) {
            $question = new Question();
            $question->setAttribute('question', $questionData['question']);
            if(isset($questionData['extra_info']) && $questionData['extra_info']){
                $question->setAttribute('extra_info', $questionData['extra_info']);
            }
            $question->setAttribute('order', $questionData['order']);

            $questionset = Questionset::findOrFail($questionData['questionset_id']);
            $question->questionset()->associate($questionset);

            $question->save();
            $questionsResponse[] = $question;
        }

        foreach ($questionset->positions as $position){
            Storage::disk('do')->put(
                'position/'.$position->id.'/questionsets/'.$questionset->id.'.pdf',
                QuestionsetController::pdf($questionset->id, $position->id),
                'public'
            );
        }

        return (new QuestionCollection($questionsResponse))->additional(['message' => 'Questions created successfully']);
    }

    /**
     * Question Show Method
     * <aside>Get info from the specified Question in storage.</aside>
     * @authenticated
     *
     * @urlParam question required The ID of the Question.
     *
     * @bodyParam extra_info string
     *
     * @responseField data object Question Resource
     *
     * @apiResource App\Http\Resources\Question
     * @apiResourceModel App\Question
     */
    public function show(Question $question)
    {
        $question->load(['questionset']);
        return new QuestionResource($question);
    }

    /**
     * Question Update Method
     *
     * <aside>Update the specified Question's info in storage.</aside>
     * @authenticated
     * @urlParam question required The ID of the Question.
     *
     * @bodyParam question string
     * @bodyParam extra_info string
     *
     * @responseField data object Question Resource
     *
     * @apiResource App\Http\Resources\Question
     * @apiResourceModel App\Question
     */
    public function update(UpdateQuestionRequest $request, Question $question)
    {
        if(!is_null($request->get('question'))){
            $question->setAttribute('question', $request->get('question'));
        }

        if(!is_null($request->get('extra_info'))){
            $question->setAttribute('extra_info', $request->get('extra_info'));
        }

        if(!is_null($request->get('order'))){
            $question->setAttribute('order', $request->get('order'));
        }

        if(!is_null($request->get('questionset_id'))){
            $questionset =  Questionset::findOrFail($request->get('questionset_id'));
            $question->questionset()->associate($questionset);
        }

        $question->save();
        $question->refresh();

        foreach ($question->questionset->positions as $position){
            Storage::disk('do')->put(
                'position/'.$position->id.'/questionsets/'.$question->questionset->id.'.pdf',
                QuestionsetController::pdf($question->questionset->id, $position->id),
                'public'
            );
        }

        return (new QuestionResource($question))->additional(['message' => 'Question updated successfully']);
    }

    /**
     * Question Delete Method
     *
     * <aside>Remove the specified Question from storage.</aside>
     * @authenticated
     *
     * @urlParam question required The ID of the Question.
     *
     * @response {
     *  "message": "Question deleted successfully"
     * }
     */
    public function destroy(Question $question)
    {
        $question->delete();
        return response()->json([
            'message' => 'Question deleted successfully'
        ]);
    }
}
