<?php

namespace App\Http\Controllers;

use App\Skill;
use App\Http\Requests\StoreSkillRequest;
use App\Http\Requests\UpdateSkillRequest;
use App\Http\Resources\SkillCollection;
use App\State;
use Illuminate\Http\Request;
use App\Http\Resources\Skill as SkillResource;
use Illuminate\Support\Facades\Hash;

/**
 * @group Skills Controller
 *
 * <aside>APIs for managing skills model</aside>
 */
class SkillController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Skill::class);
    }

    /**
     * Skill Index Method
     * 
     * <aside>Get a list of all the Skill in storage.</aside>
     * @authenticated
     * 
     * @responseField data array Skill Collection Resource
     * 
     * @apiResourceCollection App\Http\Resources\SkillCollection
     * @apiResourceModel App\Skill
     */
    public function index()
    {
        return new SkillCollection(Skill::all());
    }

    /**
     * Skill Create Method
     * 
     * <aside>Create a new Skill entry in storage</aside>
     * @authenticated
     * 
     * @bodyParam description string
     * 
     * @responseField data object Skill Resource
     * 
     * @apiResource App\Http\Resources\Skill
     * @apiResourceModel App\Skill
     */
    public function store(StoreSkillRequest $request)
    {
        $skill = new Skill();
        $skill->setAttribute('name', $request->get('name'));

        if(!is_null($request->get('description'))){
            $skill->setAttribute('description', $request->get('description'));
        }

        $skill->save();
        return (new SkillResource($skill))->additional(['message' => 'Skill created successfully']);
    }

    /**
     * Skill Show Method
     * <aside>Get info from the specified Skill in storage.</aside>
     * @authenticated
     *
     * @urlParam Skill required The ID of the Skill.
     * 
     * @responseField data object Skill Resource
     * 
     * @apiResource App\Http\Resources\Skill
     * @apiResourceModel App\Skill
     */
    public function show(Skill $skill)
    {
        return new SkillResource($skill);
    }

    /**
     * Skill Update Method
     * 
     * <aside>Update the specified Skill's info in storage.</aside>
     * @authenticated
     * @urlParam skill required The ID of the Skill.
     * 
     * @bodyParam description string
     * 
     * @responseField data object Skill Resource
     * 
     * @apiResource App\Http\Resources\Skill
     * @apiResourceModel App\Skill
     */
    public function update(UpdateSkillRequest $request, Skill $skill)
    {
        if(!is_null($request->get('name'))){
            $skill->setAttribute('name', $request->get('name'));
        }

        if(!is_null($request->get('description'))){
            $skill->setAttribute('description', $request->get('description'));
        }
        
        $skill->save();

        return (new SkillResource($skill))->additional(['message' => 'Skill updated successfully']);
    }

    /**
     * Skill Delete Method
     * 
     * <aside>Remove the specified Skill from storage.</aside>
     * @authenticated
     * 
     * @urlParam skill required The ID of the Skill.
     * 
     * @response {
     *  "message": "Skill deleted successfully"
     * }
     */
    public function destroy(Skill $skill)
    {
        $skill->delete();
        return response()->json([
            'message' => 'Skill deleted successfully'
        ]);
    }
}
