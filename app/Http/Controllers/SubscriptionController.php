<?php

namespace App\Http\Controllers;

use App\Position;
use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreSubscriptionRequest;
use App\Http\Requests\UpdateSubscriptionRequest;
use App\Http\Resources\SubscriptionCollection;
use App\Http\Resources\Subscription as SubscriptionResource;


/**
 * @group Subscription Controller
 *
 * <aside>APIs for managing Subscription model</aside>
 */
class SubscriptionController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Subscription::class);
    }

    /**
     * Subscription Index Method
     *
     * <aside>Get a list of all the Subscription in storage.</aside>
     * @authenticated
     *
     * @responseField data array Subscription Collection Resource
     *
     * @apiResourceCollection App\Http\Resources\SubscriptionCollection
     * @apiResourceModel App\Subscription
     */
    public function index()
    {
        $user = Auth::user();
        $subscriptions;
        if ($user->hasRole('super-admin')) {
            $subscriptions = Subscription::all();
        } else {
            $subscriptions = Subscription::where('user_id', $user->id)->get();
        }
        return new SubscriptionCollection($subscriptions);
    }

    /**
     * Subscription Create Method
     *
     * <aside>Create a new Subscription entry in storage</aside>
     * @authenticated
     *
     * @responseField data object Subscription Resource
     *
     * @apiResource App\Http\Resources\Subscription
     * @apiResourceModel App\Subscription
     */
    public function store(StoreSubscriptionRequest $request)
    {
        $subscription = new Subscription();

        $position =  Position::findOrFail($request->get('position_id'));
        $subscription->subscribable()->associate($position);

        $user = Auth::user();
        $subscription->user()->associate($user);

        $subscription->save();
        return new SubscriptionResource($subscription);
    }

    /**
     * Subscription Show Method
     *
     * <aside>Get info from the specified Subscription in storage.</aside>
     * @authenticated
     *
     * @urlParam subscription required The ID of the Subscription.
     *
     * @responseField data object Subscription Resource
     *
     * @apiResource App\Http\Resources\Subscription
     * @apiResourceModel App\Subscription
     */
    public function show(Subscription $subscription)
    {
        return new SubscriptionResource($subscription);
    }

    /**
     * Subscription Update Method
     *
     * <aside>Update the specified Subscription's info in storage.</aside>
     *
     * @authenticated
     * @urlParam subscription required The ID of the Subscription.
     *
     * @responseField data object Subscription Resource
     *
     * @apiResource App\Http\Resources\Subscription
     * @apiResourceModel App\Subscription
     */
    public function update(UpdateSubscriptionRequest $request, Subscription $subscription)
    {
        if(!is_null($request->get('position_id'))){
            $subscription->setAttribute('position_id', $request->get('position_id'));
        }

        $subscription->save();
        return (new SubscriptionResource($subscription))->additional(['message' => 'Subscription updated successfully']);
    }

    /**
     * Subscription Delete Method
     *
     * <aside>Remove the specified Subscription from storage.</aside>
     * @authenticated
     *
     * @urlParam subscription required The ID of the Subscription.
     *
     * @response {
     *  "message": "Subscription deleted successfully"
     * }
     */
    public function destroy(Subscription $subscription)
    {
        $subscription->delete();
        return response()->json([
            'message' => 'Subscription deleted successfully'
        ]);
    }
}
