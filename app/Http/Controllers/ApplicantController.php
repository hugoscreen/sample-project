<?php

namespace App\Http\Controllers;

use App\User;
use App\Email;
use App\State;
use App\Applicant;
use App\Notification;
use App\Http\Requests\StoreApplicantRequest;
use App\Http\Requests\UpdateApplicantRequest;
use App\Http\Resources\ApplicantCollection;
use App\Http\Resources\Applicant as ApplicantResource;
use Illuminate\Http\Request;

/**
 * @group Applicant Controller
 *
 * <aside>APIs for managing applicant model</aside>
 */
class ApplicantController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Applicant::class);
    }

    /**
     * Applicant Index Method
     *
     * <aside>Get a list of all the Applicant in storage.</aside>
     * @authenticated
     *
     * @responseField data array Applicant Collection Resource
     *
     * @apiResourceCollection App\Http\Resources\ApplicantCollection
     * @apiResourceModel App\Applicant
     */
    public function index()
    {
        return new ApplicantCollection(Applicant::all());
    }

    /**
     * Applicant Create Method
     *
     * <aside>Create a new User and new Applicant entry in storage</aside>
     * @authenticated
     *
     * @responseField data object Applicant Resource
     *
     * @apiResource App\Http\Resources\Applicant
     * @apiResourceModel App\Applicant
     */
    public function store(StoreApplicantRequest $request)
    {

        $user = UserController::createUser($request);
        $applicant = new Applicant();

        if(!is_null($request->get('linkedin'))){
            $applicant->setAttribute('linkedin', $request->get('linkedin'));
        }

        if(!is_null($request->get('github'))){
            $applicant->setAttribute('github', $request->get('github'));
        }

        if(!is_null($request->get('stackoverflow'))){
            $applicant->setAttribute('stackoverflow', $request->get('stackoverflow'));
        }

        $user->syncRoles(['applicant']);
        $applicant->user()->associate($user);

        $applicant->save();

        $email = new Email();
        $email->template = 'new-aplicant';
        $email->user_id = $user->id;
        $email->subject = 'Welcome to Collok.io';

        $email->data = [
            'name' => $user->name,
            'mail' => $user->email,
            'password' => $user->unhashPassword
        ];
        $emailResponse = $email->sendMail();

        return (new ApplicantResource($applicant))->additional(['message' => 'Applicant created successfully']);
    }

    /**
     * Applicant Show Method
     * <aside>Get info from the specified Applicant in storage.</aside>
     * @authenticated
     *
     * @urlParam applicant required The ID of the Applicant.
     *
     * @responseField data object Applicant Resource
     *
     * @apiResource App\Http\Resources\Applicant
     * @apiResourceModel App\Applicant
     */
    public function show(Applicant $applicant)
    {
        $applicant->load('applications');
        return new ApplicantResource($applicant);
    }

    /**
     * Applicant Update Method
     *
     * <aside>Update the specified Applicant's info in storage.</aside>
     * @authenticated
     * @urlParam applicant required The ID of the Applicant.
     *
     * @responseField data object Applicant Resource
     *
     * @apiResource App\Http\Resources\Applicant
     * @apiResourceModel App\Applicant
     */
    public function update(UpdateApplicantRequest $request, Applicant $applicant)
    {
        if(!is_null($request->get('linkedin'))){
            $applicant->setAttribute('linkedin', $request->get('linkedin'));
        }

        if(!is_null($request->get('github'))){
            $applicant->setAttribute('github', $request->get('github'));
        }

        if(!is_null($request->get('stackoverflow'))){
            $applicant->setAttribute('stackoverflow', $request->get('stackoverflow'));
        }

        if(!is_null($request->get('user_id'))){
            //removing permissions from old associated user
            $applicant->user->removeRole('applicant');
            $user =  User::findOrFail($request->get('user_id'));
            $user->syncRoles(['applicant']);
            $applicant->user()->associate($user);
        }

        $applicant->save();

        return (new ApplicantResource($applicant))->additional(['message' => 'Applicant updated successfully']);
    }

    /**
     * Applicant Delete Method
     *
     * <aside>Remove the specified Applicant from storage.</aside>
     * @authenticated
     *
     * @urlParam applicant required The ID of the Applicant.
     *
     * @response {
     *  "message": "Applicant deleted successfully"
     * }
     */
    public function destroy(Applicant $applicant)
    {
        $applicant->user()->delete();
        $applicant->delete();
        return response()->json([
            'message' => 'Applicant deleted successfully'
        ]);
    }
}
