<?php

namespace App\Http\Controllers;

use App\BBB;
use App\Application;
use App\Contractor;
use App\Http\Requests\StoreLiveinterviewRequest;
use App\Liveinterview;
use App\Recruiter;
use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\GetRecordingsParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


/**
 * @group BigBlueButton
 *
 * <aside>APIs for managing BigBlueButton Resources</aside>
 */
class BBBController extends Controller
{

    /**
     * Get Recordings Method
     *
     * <aside>Join to a rehearsal room.</aside>
     * @authenticated
     *
     * @urlParam id required The BBBinternalId of the LiveInterview or OndemandInterview.
     *
     * @response {
     *  "data": [
     *      {
     *          "playback": "URL",
     *      }
     *  ]
     * }
     *
     *
     * @response scenario="No recordings available" {
     *  "message": "No recordings available"
     * }
     */
    public function getRecordings($meetingId)
    {
        $bbb = new BigBlueButton();
        $recordingParams = new GetRecordingsParameters();
        $recordingParams->setMeetingId($meetingId);
        $response = $bbb->getRecordings($recordingParams);
        if(empty($response->getRecords())){
            return response()->json(['message'=>'No recordings available']);
        }

        $xmlArray = json_decode(json_encode($response->getRawXml()), true);
        $returnArray=[];
        foreach ($xmlArray['recordings'] as $recording){
            $returnArray[]['playback'] = $recording['playback']['format']['url'];
        }

        return response()->json($returnArray);
    }

    /**
     * Join Rehearsal Meeting Method
     *
     * <aside>Join to a rehearsal room.</aside>
     * @authenticated
     *
     *
     * @responseField message string
     * @responseField meetingURL Valid URL of the rehearsal room
     *
     * @response {
     *  "message": "Successful url fetch for live join",
     *  "meetingURL": "validURL"
     * }
     */
    public function joinRehearsalMeeting() {
        $user = Auth::user();
        $BBB = new BBB();
        $test_id = 'test'.uniqid('', true);
        $positionTemp = (object) ['name' => 'Rehearsal Room for '.$user->getAttribute('name')];
        $meeting_id = $BBB->createRoom($test_id, $positionTemp , null, false, true);
        $response = $BBB->joinMeeting($test_id, $user, true, true);
        return response()->json($response);
    }
}
