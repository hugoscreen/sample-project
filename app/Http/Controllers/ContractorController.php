<?php

namespace App\Http\Controllers;

use App\Company;
use App\Contractor;
use App\Email;
use App\Http\Requests\StoreContractorRequest;
use App\Http\Requests\UpdateContractorRequest;
use App\Http\Resources\ContractorCollection;
use App\State;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\Contractor as ContractorResource;

/**
 * @group Contractor Controller
 *
 * <aside>APIs for managing contractor model</aside>
 */
class ContractorController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Contractor::class);
    }

    /**
     * Contractor Index Method
     *
     * <aside>Get a list of all the Contractors in storage.</aside>
     * @authenticated
     *
     * @responseField data array Contractor Collection Resource
     *
     * @apiResourceCollection App\Http\Resources\ContractorCollection
     * @apiResourceModel App\Contractor
     */
    public function index()
    {
        return new ContractorCollection(Contractor::all());
    }

    /**
     * Contractor Create Method
     *
     * <aside>Create a new User and new Contractor entry in storage</aside>
     * @authenticated
     *
     * @responseField data object Contractor Resource
     *
     * @apiResource App\Http\Resources\Contractor
     * @apiResourceModel App\Contractor
     */
    public function store(StoreContractorRequest $request)
    {
        $user = UserController::createUser($request);
        $contractor = new Contractor();

        if(!is_null($request->get('position'))){
            $contractor->setAttribute('position', $request->get('position'));
        }

        $user->syncRoles(['contractor']);
        $contractor->user()->associate($user);

        $company =  Company::findOrFail($request->get('company_id'));
        $contractor->company()->associate($company);

        //generating notification
        $email = new Email();
        $email->user_id = $user->id;
        $email->url = env('FRONT_URL', 'https://collok.io')."/login?email=".$user->email;
        $email->subject = "Collok.io - New User Credetials";
        $email->message = 'Hello, Contractor '. $contractor->user->name.':<br/><br/>';
        $email->message .= 'You have received an email from the Collok.io platform.<br/><br/>';
        $email->message .= "The system has generated a user for you, you can access with the following data:<br/>";
        $email->message .= "Username: {$user->email}<br/>";
        $email->message .= "Pasword: {$user->unhashPassword}<br/>";
        // TODO: generate persistent notifications
        $email->sendMail(false);

        $contractor->save();
        return (new ContractorResource($contractor))->additional(['message' => 'Contractor created successfully']);
    }

    /**
     * Contractor Show Method
     * <aside>Get info from the specified Contractor in storage.</aside>
     * @authenticated
     *
     * @urlParam contractor required The ID of the Contractor.
     *
     * @responseField data object Contractor Resource
     *
     * @apiResource App\Http\Resources\Contractor
     * @apiResourceModel App\Contractor
     */
    public function show(Contractor $contractor)
    {
        $contractor->load('company','user');
        return new ContractorResource($contractor);
    }

    /**
     * Contractor Update Method
     *
     * <aside>Update the specified Contractor's info in storage.</aside>
     * @authenticated
     * @urlParam contractor required The ID of the Contractor.
     *
     * @bodyParam position string
     *
     * @responseField data object Contractor Resource
     *
     * @apiResource App\Http\Resources\Contractor
     * @apiResourceModel App\Contractor
     */
    public function update(UpdateContractorRequest $request, Contractor $contractor)
    {
        if(!is_null($request->get('position'))){
            $contractor->setAttribute('position', $request->get('position'));
        }

        if(!is_null($request->get('user_id'))){
            //removing permissions from old associated user
            $contractor->user->removeRole('contractor');
            $user =  User::findOrFail($request->get('user_id'));
            $user->syncRoles(['contractor']);
            $contractor->user()->associate($user);
        }

        if(!is_null($request->get('company_id'))){
            //removing permissions from old associated user
            $company =  Company::findOrFail($request->get('company_id'));
            $contractor->company()->associate($company);
        }

        $contractor->save();

        return (new ContractorResource($contractor))->additional(['message' => 'Contractor updated successfully']);
    }

    /**
     * Contractor Delete Method
     *
     * <aside>Remove the specified Contractor from storage.</aside>
     * @authenticated
     *
     * @urlParam contractor required The ID of the Contractor.
     *
     * @response {
     *  "message": "Contractor deleted successfully"
     * }
     */
    public function destroy(Contractor $contractor)
    {
        $contractor->user()->delete();
        $contractor->delete();
        return response()->json([
            'message' => 'Contractor deleted successfully'
        ]);
    }
}
