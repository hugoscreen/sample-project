<?php

namespace App\Http\Controllers;

use App\BBB;
use App\Email;
use App\Recruiter;
use App\Contractor;
use App\Application;
use App\Questionset;
use App\Notification;
use App\Ondemandinterview;
use App\Events\NewNotification;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreOndemandInterviewRequest;
use App\Http\Requests\UpdateOndemandInterviewRequest;
use App\Http\Resources\Ondemandinterview as OndemandinterviewResource;

/**
 * @group OndemandInterview Controller
 *
 * <aside>APIs for managing ondemand interview model</aside>
 */
class OndemandinterviewController extends Controller
{

    /**
     * Ondemandinterview Create Method
     *
     * <aside>Create a new Ondemandinterview entry in storage and send a mail
     * notification about the about the Ondemandinterview created</aside>
     * @authenticated
     *
     * @responseField data object Ondemandinterview Resource
     *
     * @apiResource App\Http\Resources\Ondemandinterview
     * @apiResourceModel App\Ondemandinterview
     */
    public function store(StoreOndemandInterviewRequest $request)
    {
        $application = Application::findOrFail($request->get('application_id'));
        $position = $application->getAttribute('position');

        $questionset;
        if (!is_null($request->get('questionset_id'))) {
            $questionset = Questionset::find($request->get('questionset_id'));
        } else if (!empty($position->defaultQuestionSet)) {
            $questionset = $position->defaultQuestionSet;
        } else {
            $questionset = $position->questionsets()->first();
        }

        if (!$position->questionsets->contains($questionset)) {
            abort(422, 'The questionset_id does not belong to the application (position).');
        }

        $ondemandInterview = new Ondemandinterview();
        $ondemandInterview->application()->associate($application);
        $ondemandInterview->questionset()->associate($questionset);
        if (!is_null($request->get('show_hiring_manager'))) {
            $ondemandInterview->setAttribute('show_hiring_manager', $request->get('show_hiring_manager'));
        }
        $ondemandInterview->save();
        $ondemandInterview->setAttribute('internalMeetingId', $ondemandInterview->getAttribute('id').uniqid("", true));
        $BBB = new BBB();
        $ondemandInterview->setAttribute('BBBInternalId',
            $BBB->createRoom($ondemandInterview->getAttribute('internalMeetingId'), $position, $questionset->id, true));
        $ondemandInterview->save();

        $email = new Email();
        $email->template = 'new-ondemand-applicant';
        $email->user_id = $application->applicant->user_id;
        $email->subject = 'Collok.io: New On Demand Interview for '.$application->applicant->user->name;

        $email->data = [
            'name' => $application->applicant->user->name,
            'company' => $application->position->company->name,
            'position' => $application->position->name
        ];
        $emailResponse = $email->sendMail();

        $notification = new Notification();
        $notification->setAttribute('title', 'A New On Demand Interview');
        $notification->setAttribute('text', 'You have a new on demand interview.');
        $notification->setAttribute('type', 'info');
        $notification->user()->associate($application->applicant->user);
        $notification->save();
        broadcast(new NewNotification($notification));

        return response()->json(['message' => 'created successfully', 'data' => $ondemandInterview]);
    }

    /**
     * Ondemandinterview Join Method
     *
     * <aside>Join to the specified Ondemandinterview.</aside>
     * @authenticated
     *
     * @urlParam ondemand_interview required The internalMeetingId of the Ondemandinterview.
     *
     * @responseField message string
     * @responseField meetingURL Valid URL of the ondemand interview or waiting for applicant
     *
     * @response scenario="The user authenticated has Applicant role" {
     *  "message": "Successful url fetch for live join",
     *  "meetingURL": "validURL"
     * }
     *
     * @response scenario="The user authenticated hasn't Applicant role" {
     *  "message": "The Applicant has not entered yet to record",
     *  "meetingURL": "https://collok.io/waitingForApplicant"
     * }
     */
    public function show($meeting_id)
    {
        $ondemandInterview = Ondemandinterview::where('internalMeetingId', $meeting_id)->firstOrFail();
        $user = Auth::user();
        if(!$user->hasRole('applicant')){
            return response()->json(['message'=>'The Applicant has not entered yet to record', 'meetingURL'=>env('FRONT_URL', 'https://collok.io').'/waitingForApplicant']);
        }
        $BBB = new BBB();
        $response = $BBB->joinMeeting($meeting_id, $user, true);

        $email = new Email();
        $email->template = 'new-interview-recruiter';
        $email->subject = 'Collok.io: New On Demand Interview Record of Applicant '.$ondemandInterview->application->applicant->user->name;
        $email->recipients = $ondemandInterview->application->position->subscribers;
        $email->data = [
            'position' => $ondemandInterview->application->position->name
        ];
        $emailResponse = $email->sendMultipleMails();

        foreach ($ondemandInterview->application->position->subscribers as $user) {
            $notification = new Notification();
            $notification->setAttribute('title', 'On Demand Interview Recording');
            $notification->setAttribute('text', 'Applicant'.
                $ondemandInterview->application->applicant->user->name
                .' has enter to on demand interview'
                .$ondemandInterview->application->position->name
                .' position. The recording will be available soon.');
            $notification->setAttribute('type', 'info');
            $notification->user()->associate($user);
            $notification->save();
            broadcast(new NewNotification($notification));
        }

        return response()->json($response);
    }

    /**
     * Ondemandinterview Update Method
     *
     * <aside>Update the specified Ondemandinterview's info in storage.</aside>
     * @authenticated
     * @urlParam ondemand_interview required The ID of the Ondemandinterview.
     *
     * @responseField data object Ondemandinterview Resource
     *
     * @apiResource App\Http\Resources\Ondemandinterview
     * @apiResourceModel App\Ondemandinterview
     */
    public function update(UpdateOndemandInterviewRequest $request, Ondemandinterview $ondemandInterview)
    {

        if(!is_null($request->get('status'))){
            $ondemandInterview->setAttribute('status', $request->get('status'));
        }

        if(!is_null($request->get('show_hiring_manager'))){
            $ondemandInterview->setAttribute('show_hiring_manager', $request->get('show_hiring_manager'));
        }

        $ondemandInterview->save();

        return (new OndemandinterviewResource($ondemandInterview))->additional(['message' => 'OndemandInterview updated successfully']);
    }

}
