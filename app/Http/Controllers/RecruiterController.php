<?php

namespace App\Http\Controllers;

use App\Company;
use App\Recruiter;
use App\Http\Requests\StoreRecruiterRequest;
use App\Http\Requests\UpdateRecruiterRequest;
use App\Http\Resources\RecruiterCollection;
use App\Email;
use App\State;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\Recruiter as RecruiterResource;

/**
 * @group Recruiter Controller
 *
 * <aside>APIs for managing recruiter model</aside>
 */
class RecruiterController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Recruiter::class);
    }

    /**
     * Recruiter Index Method
     *
     * <aside>Get a list of all the Recruiter in storage.</aside>
     * @authenticated
     *
     * @responseField data array Recruiter Collection Resource
     *
     * @apiResourceCollection App\Http\Resources\RecruiterCollection
     * @apiResourceModel App\Recruiter
     */
    public function index()
    {
        return new RecruiterCollection(Recruiter::all());
    }

    /**
     * Recruiter Create Method
     *
     * <aside>Create a new Recruiter entry in storage</aside>
     * @authenticated
     *
     * @bodyParam position string
     *
     * @responseField data object Recruiter Resource
     *
     * @apiResource App\Http\Resources\Recruiter
     * @apiResourceModel App\Recruiter
     */
    public function store(StoreRecruiterRequest $request)
    {
        $user = UserController::createUser($request);
        $recruiter = new Recruiter();

        if(!is_null($request->get('position'))){
            $recruiter->setAttribute('position', $request->get('position'));
        }

        $user->syncRoles(['recruiter']);
        $recruiter->user()->associate($user);
        $recruiter->save();

        $email = new Email();
        $email->template = 'new_user_recruiter';
        $email->user_id = $user->id;
        $email->subject = 'Welcome to Collok.io';

        $email->data = [
            'name' => $user->name,
            'mail' => $user->email,
            'password' => $user->unhashPassword
        ];
        $emailResponse = $email->sendMail();
        return (new RecruiterResource($recruiter))->additional(['message' => 'Recruiter created successfully']);
    }

    /**
     * Recruiter Show Method
     * <aside>Get info from the specified Recruiter in storage.</aside>
     * @authenticated
     *
     * @urlParam recruiter required The ID of the Recruiter.
     *
     * @responseField data object Recruiter Resource
     *
     * @apiResource App\Http\Resources\Recruiter
     * @apiResourceModel App\Recruiter
     */
    public function show(Recruiter $recruiter)
    {
        $recruiter->load('user');
        return new RecruiterResource($recruiter);
    }

    /**
     * Recruiter Update Method
     *
     * <aside>Update the specified Recruiter's info in storage.</aside>
     * @authenticated
     * @urlParam recruiter required The ID of the Recruiter.
     *
     * @responseField data object Recruiter Resource
     *
     * @apiResource App\Http\Resources\Recruiter
     * @apiResourceModel App\Recruiter
     */
    public function update(UpdateRecruiterRequest $request, Recruiter $recruiter)
    {
        if(!is_null($request->get('position'))){
            $recruiter->setAttribute('position', $request->get('position'));
        }

        if(!is_null($request->get('user_id'))){
            //removing permissions from old associated user
            $recruiter->user->removeRole('recruiter');
            $user =  User::findOrFail($request->get('user_id'));
            $user->syncRoles(['recruiter']);
            $recruiter->user()->associate($user);
        }

        $recruiter->save();

        return (new RecruiterResource($recruiter))->additional(['message' => 'Recruiter updated successfully']);
    }

    /**
     * Recruiter Delete Method
     *
     * <aside>Remove the specified Recruiter from storage.</aside>
     * @authenticated
     *
     * @urlParam recruiter required The ID of the Recruiter.
     *
     * @response {
     *  "message": "Recruiter deleted successfully"
     * }
     */
    public function destroy(Recruiter $recruiter)
    {
        $recruiter->user()->delete();
        $recruiter->delete();
        return response()->json([
            'message' => 'Recruiter deleted successfully'
        ]);
    }
}
