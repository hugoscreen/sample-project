<?php

namespace App\Http\Controllers;

use App\EmailLog;
use App\Http\Resources\EmailLog as EmailLogResource;
use App\Http\Resources\EmailLogCollection;
use Illuminate\Http\Request;

/**
 * @group EmailLog Controller
 *
 * <aside>APIs for managing EmailLog model</aside>
 */
class EmailLogController extends Controller
{
    /**
     * EmailLog Index Method
     * 
     * <aside>Get a list of all the EmailLog in storage.</aside>
     * @authenticated
     * 
     * @responseField data array EmailLog Collection Resource
     * 
     * @apiResourceCollection App\Http\Resources\EmailLogCollection
     * @apiResourceModel App\EmailLog
     */
    public function index()
    {
        return new EmailLogCollection(EmailLog::all());
    }

    /**
     * EmailLog Show Method
     * 
     * <aside>Get info from the specified EmailLog in storage.</aside>
     * @authenticated
     *
     * @urlParam email_log required The ID of the EmailLog.
     * 
     * @responseField data object EmailLog Resource
     * 
     * @apiResource App\Http\Resources\EmailLog
     * @apiResourceModel App\EmailLog
     */

    public function show(EmailLog $emailLog)
    {
        return new EmailLogResource($emailLog);
    }
}