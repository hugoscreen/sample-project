<?php

namespace App\Http\Controllers;

use App\Country;
use App\Http\Requests\StoreCountryRequest;
use App\Http\Requests\UpdateCountryRequest;
use App\Http\Resources\CountryCollection;
use App\State;
use Illuminate\Http\Request;
use App\Http\Resources\Country as CountryResource;
use Illuminate\Support\Facades\Hash;

/**
 * @group Country Controller
 *
 * <aside>APIs for managing country model</aside>
 */
class CountryController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Country::class);
    }

    /**
     * Country Index Method
     * 
     * <aside>Get a list of all the Country in storage.</aside>
     * @authenticated
     * 
     * @responseField data array Country Collection Resource
     * 
     * @apiResourceCollection App\Http\Resources\CountryCollection
     * @apiResourceModel App\Country
     */
    public function index()
    {
        return new CountryCollection(Country::all());
    }

    /**
     * Country Create Method
     * 
     * <aside>Create a new Country entry in storage</aside>
     * @authenticated
     * 
     *
     * @responseField data object Country Resource
     * 
     * @apiResource App\Http\Resources\Country
     * @apiResourceModel App\Country
     */
    public function store(StoreCountryRequest $request)
    {
        $country = new Country();
        $country->setAttribute('name', $request->get('name'));
        $country->setAttribute('code', $request->get('code'));
        $country->save();
        return (new CountryResource($country))->additional(['message' => 'Country created successfully']);
    }

    /**
     * Country Show Method
     * <aside>Get info from the specified Country in storage.</aside>
     * @authenticated
     *
     * @urlParam country required The ID of the Country.
     * 
     * @responseField data object Country Resource
     * 
     * @apiResource App\Http\Resources\Country
     * @apiResourceModel App\Country
     */
    public function show(Country $country)
    {
        return new CountryResource($country);
    }

    /**
     * Country Update Method
     * 
     * <aside>Update the specified Country's info in storage.</aside>
     * @authenticated
     * @urlParam country required The ID of the Country.
     * 
     * @bodyParam name string
     * 
     * @responseField data object Country Resource
     * 
     * @apiResource App\Http\Resources\Country
     * @apiResourceModel App\Country
     */
    public function update(UpdateCountryRequest $request, Country $country)
    {
        if(!is_null($request->get('name'))){
            $country->setAttribute('name', $request->get('name'));
        }

        if(!is_null($request->get('code'))){
            $country->setAttribute('code', $request->get('code'));
        }

        $country->save();

        return (new CountryResource($country))->additional(['message' => 'Country updated successfully']);
    }

    /**
     * Country Delete Method
     * 
     * <aside>Remove the specified Country from storage.</aside>
     * @authenticated
     * 
     * @urlParam country required The ID of the Country.
     * 
     * @response {
     *  "message": "Country deleted successfully"
     * }
     */
    public function destroy(Country $country)
    {
        $country->delete();
        return response()->json([
            'message' => 'Country deleted successfully'
        ]);
    }
}
