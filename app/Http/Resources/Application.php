<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Resources\Json\JsonResource;

class Application extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'applicant' => new Applicant($this->whenLoaded('applicant')),
            'position' => new Position($this->whenLoaded('position')),
            'skills' => new SkillCollection($this->whenLoaded('skills')),
            'languages' => new LanguageCollection($this->whenLoaded('languages')),
            'liveinterviews' => new LiveinterviewCollection(
                $this->liveinterviews()
                ->when(Auth::user()->hasRole('contractor'), function ($query) {
                    return $query->where('show_hiring_manager', 1);
                })
                ->get()
            ),
            'ondemandinterviews' => new OndemandinterviewCollection($this->ondemandinterviews()
                ->when(Auth::user()->hasRole('contractor'), function ($query) {
                    return $query->where('show_hiring_manager', 1);
                })
                ->get()
            ),
            'documents' => new DocumentCollection($this->documents)
        ];
    }
}
