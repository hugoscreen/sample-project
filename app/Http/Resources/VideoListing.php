<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VideoListing extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $shareableLink = $this->shareableLinks()->where('active', 1)
        ->whereDate('expires_at', '>', now())->first();
        $url = null;
        if (!empty($shareableLink)) {
            $url = $shareableLink->url;
        }
        
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'shareableLink' => $url,
            'liveinterviews' => new LiveinterviewCollection($this->liveinterviews),
            'ondemandinterviews' => new OndemandinterviewCollection($this->ondemandinterviews)
        ];
    }
}
