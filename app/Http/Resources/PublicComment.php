<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PublicComment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $name = null;
        if (!empty($this->recruiter)) {
            $name = $this->recruiter->user->name;
        }

        if (!empty($this->contractor)) {
            $name = $this->contractor->user->name;
        }

        if(empty($name)){
            $name = $this->user_name;
        }

        return [
            'comment' =>$this->comment,
            'type' =>$this->type,
            'name' => $name,
            'position' => $this->user_role_position
        ];
    }
}
