<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Company extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'city' => $this->city,
            'state' => $this->state->name,
            'country' => $this->country->name,
            'created_at' => $this->created_at,
            'video' => $this->video,
            'profile_picture_link' => $this->profileImageLink(),
            'positions' => new PositionCollection($this->whenLoaded('positions')),
            'contractors'=>new ContractorCollection($this->contractors),
        ];
    }
}
