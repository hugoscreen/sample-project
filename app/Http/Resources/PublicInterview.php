<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PublicInterview extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'applicantName' => $this->application->applicant->user->name,
            'profile_picture_link'=>$this->application->applicant->user->profileImageLink(),
            'positionName' => $this->application->position->name,
            'video' => $this->getRecording(),
            'comments' => new PublicCommentCollection($this->comments),
        ];
    }
}
