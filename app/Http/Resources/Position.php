<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Resources\Json\JsonResource;

class Position extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'video' => $this->video,
            'company' => new Company($this->company),
            'applications' => new ApplicationCollection($this->whenLoaded('applications', function() {
                if (Auth::user()->hasRole('contractor')) {
                    return $this->applications()
                    ->where(function($query) {
                        $query->whereHas('liveinterviews', function (Builder $query) {
                            $query->where('show_hiring_manager', '=', 1);
                        })
                        ->orWhereHas('ondemandinterviews', function (Builder $query) {
                            $query->where('show_hiring_manager', 1);
                        });
                    })
                    ->get()->load('applicant');
                } else {
                    return $this->applications->load('applicant');
                }
            })),
            'skills' => new SkillCollection($this->skills),
            'languages' => new LanguageCollection($this->languages),
            'questionsets' => new QuestionsetCollection($this->whenLoaded('questionsets')),
            'defaultQuestionSet' => new Questionset($this->whenLoaded('defaultQuestionSet')),
        ];
    }
}
