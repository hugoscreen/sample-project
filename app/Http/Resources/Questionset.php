<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Questionset extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'instructions' => $this->instructions,
            'is_public' => $this->is_public,
            'user' => $this->user,
            'positions' => new PositionCollection($this->whenLoaded('positions')),
            'questions' => new QuestionCollection($this->whenLoaded('questions')),
        ];
    }
}
