<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Language extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'level' => $this->whenPivotLoaded('position_language', function () {
                return $this->pivot->level;
            }),
            'contractor' => $this->whenPivotLoaded('application_language', function () {
                return new Contractor(\App\Contractor::find($this->pivot->contractor_id));
            }),
            'evaluation' => $this->whenPivotLoaded('application_language', function () {
                return $this->pivot->evaluation;
            }),
        ];
    }
}
