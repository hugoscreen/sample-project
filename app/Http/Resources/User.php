<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'roles' =>  $this->whenLoaded('roles'),
            'profile_picture_link' => $this->profileImageLink(),
            'recruiter' => new Recruiter($this->whenLoaded('recruiter')),
            'contractor' => new Contractor($this->whenLoaded('contractor')),
            'applicant' => new Applicant($this->whenLoaded('applicant')),
        ];
    }
}
