<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Subscription extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $class = str_replace("App\\",'', get_class($this->subscribable));
        $key = strtolower($class);
        $className = __NAMESPACE__.'\\'.$class;

        return [
            'id' => $this->id,
            // This allows to dynamically set a Model with dynamic key and Resource
            $key => new $className($this->subscribable),
            'user' => new User($this->user)
        ];
    }
}
