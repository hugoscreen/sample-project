<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Skill extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'level' => $this->whenPivotLoaded('position_skill', function () {
                return $this->pivot->level;
            }),
            'contractor' => $this->whenPivotLoaded('application_skill', function () {
                return new Contractor(\App\Contractor::find($this->pivot->contractor_id));
            }),
            'evaluation' => $this->whenPivotLoaded('application_skill', function () {
                return $this->pivot->evaluation;
            }),
        ];
    }
}
