<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Resources\Json\JsonResource;

class Applicant extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'linkedin' => $this->linkedin,
            'github' => $this->github,
            'stackoverflow' => $this->stackoverflow,
            'user' => new User($this->user),
            'applications' => new ApplicationCollection($this->whenLoaded('applications', function() {
                if (Auth::user()->hasRole('contractor')) {
                    return $this->applications()
                    ->where(function($query) {
                        $query->whereHas('liveinterviews', function (Builder $query) {
                            $query->where('show_hiring_manager', '=', 1);
                        })
                        ->orWhereHas('ondemandinterviews', function (Builder $query) {
                            $query->where('show_hiring_manager', 1);
                        });
                    })
                    ->get()->load('position');
                } else {
                    return $this->applications->load('position');
                }
            }))
        ];
    }
}
