<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Liveinterview extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id'=>$this->id,
            'created_at'=>$this->created_at,
            'scheduled'=>$this->scheduled,
            'internalMeetingId'=>$this->internalMeetingId,
            'status'=>$this->meetingStatus(),
            'lastVideo'=>$this->getLastVideo(),
            'comments'=>new CommentCollection($this->comments),
            'show_hiring_manager'=>$this->show_hiring_manager,
            'applicantName' => $this->application->applicant->user->name,
            'positionName' => $this->application->position->name,
        ];
    }
}
