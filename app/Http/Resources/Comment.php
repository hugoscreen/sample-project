<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Comment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'comment' =>$this->comment,
            'type' =>$this->type,
            'contractor' => new Contractor($this->contractor),
            'recruiter' => new Recruiter($this->recruiter),
            'name' => $this->user_name,
            'position' => $this->user_role_position,
            'liveinterview' => new Recruiter($this->liveinterview),
            'ondemandinterview' => new Recruiter($this->ondemandinterview),
        ];
    }
}
