<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Question extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'question' => $this->question,
            'extra_info' => $this->extra_info,
            'order' => $this->order,
            'questionset' => new Questionset($this->whenLoaded('questionset')),
        ];
    }
}
