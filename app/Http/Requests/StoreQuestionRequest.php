<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreQuestionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question' => 'required_without:questions',
            'order' => 'required_without:questions|numeric',
            'questionset_id' => 'required_without:questions|numeric|exists:App\Questionset,id',
            'questions' => 'array|required_without_all:question,order,questionset_id',
            'questions.*.question' => 'required',
            'questions.*.order' => 'required|numeric',
            'questions.*.questionset_id' => 'required|numeric|exists:App\Questionset,id',
        ];
    }
}
