<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateApplicationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'in:new,recruiter_review,contractor_review,on_hold,needs_more_info,on_offer,declined,accepted,rejected,canceled',
            'applicant_id' => 'numeric|exists:App\Applicant,id',
            'position_id' => 'numeric|exists:App\Position,id'
        ];
    }

}
