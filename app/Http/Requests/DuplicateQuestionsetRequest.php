<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DuplicateQuestionsetRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'questionset_id' => 'required|integer|exists:App\Questionset,id',
            'position_id' => 'required|integer|exists:App\Position,id'
        ];
    }
}
