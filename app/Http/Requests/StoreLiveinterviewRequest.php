<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLiveinterviewRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'application_id'=>'required|numeric|exists:App\Application,id',
            'recruiter_id'=>'required|numeric|exists:App\Recruiter,id',
            'contractor_id'=>'required|numeric|exists:App\Contractor,id',
            'scheduled'=>'date',
            'show_hiring_manager' => 'boolean',
        ];
    }
}
