<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreQuestionsetRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'instructions' => 'required',
            'is_public' => 'boolean',
            'positions' => 'array',
            'positions.*' => 'required|numeric|exists:App\Position,id',
        ];
    }
}
