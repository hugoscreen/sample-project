<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCommentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'in:positive,neutral,negative',
            'contractor_id' => 'numeric|exists:App\Contractor,id',
            'recruiter_id' => 'numeric|exists:App\Recruiter,id',
            'liveinterview_id' => 'numeric|exists:App\Liveinterview,id',
            'ondemandinterview_id' => 'numeric|exists:App\Ondemandinterview,id',
        ];
    }

}
