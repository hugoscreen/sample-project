<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOndemandInterviewRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'application_id'=>'required|numeric|exists:App\Application,id',
            'questionset_id'=>'numeric|exists:App\Questionset,id',
            'show_hiring_manager' => 'boolean',
        ];
    }
}
