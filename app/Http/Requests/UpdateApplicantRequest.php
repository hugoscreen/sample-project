<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateApplicantRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'numeric|exists:App\User,id',
            'linkedin' => 'string',
            'github' => 'string',
            'stackoverflow' => 'string'
        ];
    }
}
