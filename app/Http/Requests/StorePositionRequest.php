<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePositionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required|string',
            'company_id' => 'required|numeric|exists:App\Company,id',
            'languages' => 'array',
            'languages.*.id' => 'required|numeric|exists:App\Language,id',
            'languages.*.level' => 'required|in:basic,intermediate,advanced,native',
            'skills' => 'array',
            'skills.*.skill' => 'required|string',
            'skills.*.level' => 'required|in:basic,junior,senior,expert',
        ];
    }

}
