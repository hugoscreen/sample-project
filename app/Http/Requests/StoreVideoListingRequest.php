<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreVideoListingRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string',
            'description' => 'string',
            'description' => 'string',
            'liveinterviews' => 'array',
            'liveinterviews.*' => 'required|integer|exists:App\Liveinterview,id',
            'ondemandinterviews' => 'array',
            'ondemandinterviews.*' => 'required|integer|exists:App\Ondemandinterview,id'
        ];
    }
}
