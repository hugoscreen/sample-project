<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sassnowski\LaravelShareableModel\Shareable\Shareable;
use Sassnowski\LaravelShareableModel\Shareable\ShareableInterface;

class VideoListing extends Model  implements ShareableInterface
{
    use SoftDeletes, Shareable;
    
    public $timestamps = true;

    
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function liveinterviews()
    {
        return $this->morphedByMany('App\Liveinterview', 'videolistable', 'videolistables', 'videolisting_id', 'videolistable_id');
    }

    public function ondemandinterviews()
    {
        return $this->morphedByMany('App\Ondemandinterview', 'videolistable', 'videolistables', 'videolisting_id', 'videolistable_id');
    }

    public function shareableLinks()
    {
        return $this->morphMany('App\ShareableLink', 'shareable');
    }
}
