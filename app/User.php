<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function contractor()
    {
        return $this->hasOne('App\Contractor', 'user_id');
    }

    public function recruiter()
    {
        return $this->hasOne('App\Recruiter', 'user_id');
    }

    public function applicant()
    {
        return $this->hasOne('App\Applicant', 'user_id');
    }

    public function videolistings()
    {
        return $this->hasMany('App\VideoListing', 'user_id');
    }

    public function subscriptions()
    {
        return $this->hasMany('App\Subscription');
    }

    public function questionsets()
    {
        return $this->hasMany('App\Questionset');
    }

    public function profileImageLink()
    {
        if (Storage::disk('do')->missing($this->getAttribute('profile_picture'))) {
            return null;
        }

        return Storage::disk('do')->temporaryUrl(
            $this->getAttribute('profile_picture'), now()->addMinutes(2)
        );
    }
}
