<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    public function contractor()
    {
        return $this->belongsTo('App\Contractor', 'contractor_id');
    }

    public function recruiter()
    {
        return $this->belongsTo('App\Recruiter', 'recruiter_id');
    }

    public function liveinterview()
    {
        return $this->belongsTo('App\Liveinterview', 'liveinterview_id');
    }

    public function ondemandinterview()
    {
        return $this->belongsTo('App\Ondemandinterview', 'ondemandinterview_id');
    }

}
