<?php

namespace App;

use App\EmailLog;
use Illuminate\Database\Eloquent\Model;
use Mailgun\Mailgun;

class Email extends Model
{
    // Relationships
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function sendMail($useTemplate = true)
    {
        $emailData = $this;

        $emailLog = new EmailLog();
        $emailLog->setAttribute('sender', 'noresponse@collok.io');
        $emailLog->setAttribute('recipient', $emailData->user->email);
        $emailLog->setAttribute('subject', $emailData->subject);
        $emailLog->setAttribute('template', $emailData->template);
        $data = $emailData->data;

        $domain = env('MAILGUN_DOMAIN');
        $params = [
            'from'                  => 'Collok.io Platform <noresponse@collok.io>',
            'to'                    => $emailData->user->email,
            'subject'               => $emailData->subject,
        ];

        if ($useTemplate) {
            $params['template'] = $emailData->template;
            $params['h:X-Mailgun-Variables'] = json_encode($data);

            if (array_key_exists("password", $data)) {
                unset($data['password']);
            }
            $emailLog->setAttribute('data', json_encode($data));
        } else {
            $params['html'] = '<html>'. $emailData->message .'<br><a href="'. $emailData->url .'">Log in in Collok.io</a></html>';
        }

        try {
            $mgClient = Mailgun::create(env('MAILGUN_SECRET'), 'https://'.env('MAILGUN_ENDPOINT'));
            $response = $mgClient->messages()->send($domain, $params);
            $emailLog->setAttribute('status', 'sent');
        } catch (\Exception $e) {
            $emailLog->setAttribute('status', 'failed');
        }

        $emailLog->save();
        return $response;
    }

    public function sendMultipleMails($useTemplate = true)
    {
        foreach ($this->recipients as $recipient) {
            $this->user_id = $recipient->id;
            $this->setAttribute('data', array_merge($this->data, ['name' => $recipient->name]));
            $this->sendMail($useTemplate);
        }
    }
}
