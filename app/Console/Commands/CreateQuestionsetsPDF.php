<?php

namespace App\Console\Commands;

use App\Questionset;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class CreateQuestionsetsPDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'questionsets:pdf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create the PDF for questionsets';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $questionsets = Questionset::all();
        foreach ($questionsets as $questionset ) {
            foreach ($questionset->positions as $position){
                $data['questionset'] = $questionset;
                $data['positionName'] = $position->name;
                $pdf = App::make('dompdf.wrapper');
                $pdf->loadView('pdf.ondemand_questions', $data)->setPaper('letter', 'landscape');;
                    Storage::disk('do')->put(
                        'position/'.$position->id.'/questionsets/'.$questionset->id.'.pdf',
                        $pdf->stream(),
                        'public'
                    );
            }
        }
        echo "finished";
        return 0;
    }
}
