<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Position extends Model
{
    use SoftDeletes;

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function applications()
    {
        return $this->hasMany('App\Application', 'position_id');
    }

    public function skills()
    {
        return $this->belongsToMany('App\Skill', 'position_skill')->withPivot('level');
    }

    public function languages()
    {
        return $this->belongsToMany('App\Language', 'position_language')->withPivot('level');
    }

    public function questionsets()
    {
        return $this->belongsToMany('App\Questionset', 'position_questionset');
    }

    public function defaultQuestionSet()
    {
        return $this->belongsTo('App\Questionset', 'default_questionset_id');
    }

    public function subscriptions()
    {
        return $this->morphMany('App\Subscription', 'subscribable');
    }

    public function subscribers()
    {
        return $this->hasManyThrough('App\User', 'App\Subscription', 'subscribable_id', 'id', 'id', 'user_id')->where('subscribable_type', 'App\Position');
    }
}
