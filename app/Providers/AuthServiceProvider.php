<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Company' => 'App\Policies\CompanyPolicy',
        'App\Applicant' => 'App\Policies\ApplicantPolicy',
        'App\User' => 'App\Policies\UserPolicy',
        'App\Skill' => 'App\Policies\SkillPolicy',
        'App\Language' => 'App\Policies\LanguagePolicy',
        'App\Country' => 'App\Policies\CountryPolicy',
        'App\State' => 'App\Policies\StatePolicy',
        'App\Position' => 'App\Policies\PositionPolicy',
        'App\Application' => 'App\Policies\ApplicationPolicy',
        'App\Contractor' => 'App\Policies\ContractorPolicy',
        'App\Recruiter' => 'App\Policies\RecruiterPolicy',
        'App\Comment' => 'App\Policies\CommentPolicy',
        'App\VideoListing' => 'App\Policies\VideoListingPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
    }
}
