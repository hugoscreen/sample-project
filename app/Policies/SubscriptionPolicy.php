<?php

namespace App\Policies;

use App\Subscription;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubscriptionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Allow all actions for super-admin users.
     *
     * @param  \App\User  $user
     * @param  string  $ability
     * @return mixed
     */
    public function before($user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        } else {
            return null; // fall through to the policy method.
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //Applicant only can't check subscriptions index
        if(!$user->hasRole('applicant')){
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Subscription  $subscription
     * @return mixed
     */
    public function view(User $user, Subscription $subscription)
    {
        //Recruiter and Contractors can check their owned subscriptions
        if(!$user->hasRole('applicant')){
            return $user->id == $subscription->user_id;
        }

        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //Recruiter and Contractor only can create subscriptions
        if(!$user->hasRole('applicant')){
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Subscription  $subscription
     * @return mixed
     */
    public function update(User $user, Subscription $subscription)
    {
        //Recruiter and Contractor can update their owned subscriptions
        if(!$user->hasRole('applicant')){
            return $user->id == $subscription->user_id;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Subscription  $subscription
     * @return mixed
     */
    public function delete(User $user, Subscription $subscription)
    {
        //Recruiter and Contractor can delete their owned subscriptions
        if(!$user->hasRole('applicant')){
            return $user->id == $subscription->user_id;
        }

        return false;
    }
}
