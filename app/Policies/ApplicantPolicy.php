<?php

namespace App\Policies;

use App\User;
use App\Applicant;
use Illuminate\Auth\Access\HandlesAuthorization;

class ApplicantPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Allow all actions for super-admin users.
     *
     * @param  \App\User  $user
     * @param  string  $ability
     * @return mixed
     */
    public function before($user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        } else {
            return null; // fall through to the policy method.
        }
    }

    /**
     * Determine whether the user can view any companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        // user needs to have read positions permission
        if(!$user->can('read positions') || !$user->hasRole('recruiter') ) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can view the applicant.
     *
     * @param  \App\User|null $user
     * @param  \App\Applicant  $applicant
     * @return mixed
     */
    public function view(User $user, Applicant $applicant)
    {
        // user needs to have read clients permission
        if(!$user->can('read positions')) {
            return false;
        }

        // applicants only can read their own info
        if($user->hasRole('applicant')){
            return $applicant->user_id == $user->id;
        }

        if($user->hasRole('contractor')){
            //TODO: generate restriction: contractor only can read applicants appliying in contractor's company positions
            return true;
        }

        return true;
    }

    /**
     * Determine whether the user can create companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        // user needs to have create clients permission
        if(!$user->can('create positions')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can update the applicant.
     *
     * @param  \App\User  $user
     * @param  \App\Applicant  $applicant
     * @return mixed
     */
    public function update(User $user, Applicant $applicant)
    {
        // user needs to have edit clients permission
        if(!$user->can('edit positions')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can delete the applicant.
     *
     * @param  \App\User  $user
     * @param  \App\Applicant  $applicant
     * @return mixed
     */
    public function delete(User $user, Applicant $applicant)
    {
        // user needs to have delete clients permission
        if(!$user->can('delete positions')) {
            return false;
        }

        return true;
    }

}
