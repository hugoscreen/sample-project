<?php

namespace App\Policies;

use App\User;
use App\Application;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Allow all actions for super-admin users.
     *
     * @param  \App\User  $user
     * @param  string  $ability
     * @return mixed
     */
    public function before($user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        } else {
            return null; // fall through to the policy method.
        }
    }

    /**
     * Determine whether the user can view any users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //only super-admin can view all users list
        if($user->can('admin users')){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can view the user.
     *
     * @param  \App\User|null $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $user, User $targetUser)
    {

        // User always can manage his own profile
        if($user->id == $targetUser->id){
            return true;
        }

        //recruiter only can check user accounts of applicants and contractors
        if($user->hasRole('recruiter') && ($targetUser->hasRole('applicant') || $targetUser->hasRole('contractor'))){
            return true;
        }

        /**
         * The contractor only can check user accounts of applicants
         * that have an application to the position of its company
        */
        if($user->hasRole('contractor') && $targetUser->hasRole('applicant')){
            $commonApplications = $user->contractor->company->applications->intersect($targetUser->applicant->applications);
            if ($commonApplications->isNotEmpty()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether the user can create users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        // only super-admin and recruiter can create users
        if(!$user->hasRole('recruiter')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user, User $targetUser)
    {
        // User always can manage his own profile
        if($user->id == $targetUser->id){
            return true;
        }

        // only super-admin and recruiter can update users of applicants and contractors
        if($user->hasRole('recruiter') && ($targetUser->hasRole('applicant') || $targetUser->hasRole('contractor'))) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $user, User $targetUser)
    {
        // only super-admin and recruiter can delete users
        if(!$user->hasRole('recruiter')) {
            return false;
        }

        return true;
    }

    /**
     * Resend the email of registration to another user
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function resendRegistrationEmail(User $user)
    {
        // only super-admin and recruiter can resend a registration email
        if($user->hasRole('recruiter')) {
            return true;
        }
        return false;
    }
}
