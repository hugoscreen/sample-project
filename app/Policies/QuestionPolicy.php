<?php

namespace App\Policies;

use App\User;
use App\Question;
use Illuminate\Auth\Access\HandlesAuthorization;

class QuestionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Allow all actions for super-admin users.
     *
     * @param  \App\User  $user
     * @param  string  $ability
     * @return mixed
     */
    public function before($user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        } else {
            return null; // fall through to the policy method.
        }
    }

    /**
     * Determine whether the user can view any companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        // user needs to have read questions permission
        if(!$user->hasRole('recruiter') ) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can view the question.
     *
     * @param  \App\User|null $user
     * @param  \App\Question  $question
     * @return mixed
     */
    public function view(User $user, Question $question)
    {

        // applicant can only read questions of his position applications
        if($user->hasRole('applicant')){
            $applicant = $user->applicant;
            foreach($applicant->applications as $application ) {
                if($application->position_id ==  $question->questionset->position_id) {
                    return true;
                }
            }
            return false;
        }

        if($user->hasRole('contractor')){
            //contractor only can read questions that belongs to contractor's company
            $contractor = $user->contractor;
            if ($contractor->company_id != $question->questionset->company_id) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can create companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        // only super-admins and recruiters can create questions
        if(!$user->hasRole('recruiter') ) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can update the question.
     *
     * @param  \App\User  $user
     * @param  \App\Question  $question
     * @return mixed
     */
    public function update(User $user, Question $question)
    {
        // only super-admins and recruiters can update questions
        if(!$user->hasRole('recruiter') ) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can delete the question.
     *
     * @param  \App\User  $user
     * @param  \App\Question  $question
     * @return mixed
     */
    public function delete(User $user, Question $question)
    {
        // only super-admins and recruiters can  delete questions
        if(!$user->hasRole('recruiter') ) {
            return false;
        }

        return true;
    }

}
