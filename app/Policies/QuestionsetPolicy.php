<?php

namespace App\Policies;

use App\User;
use App\Questionset;
use Illuminate\Auth\Access\HandlesAuthorization;

class QuestionsetPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Allow all actions for super-admin users.
     *
     * @param  \App\User  $user
     * @param  string  $ability
     * @return mixed
     */
    public function before($user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        } else {
            return null; // fall through to the policy method.
        }
    }

    /**
     * Determine whether the user can view any companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        // user needs to have read questionsets permission
        if(!$user->hasRole('recruiter') ) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can view the questionset.
     *
     * @param  \App\User|null $user
     * @param  \App\Questionset  $questionset
     * @return mixed
     */
    public function view(User $user, Questionset $questionset)
    {

        // applicant can only read questionsets of his position applications
        if($user->hasRole('applicant')){
            $applicant = $user->applicant;
            foreach($applicant->applications as $application ) {
                $positions = $questionset->positions;
                foreach ($positions as $position) {
                    if($application->position_id == $position->id) {
                        return true;
                    }
                }
            }
        }

        if($user->hasRole('contractor')){
            //contractor only can read questionsets that belongs to contractor's company
            $contractor = $user->contractor;
            foreach ($questionset->positions as $position) {
                if ($contractor->company_id == $position->company_id) {
                    return true;
                }
            }
        }

        // The user can read the owned question set or if the question set is public
        return $user->id == $questionset->user_id || $questionset->is_public;
    }

    /**
     * Determine whether the user can create companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        // only super-admins and recruiters can create questionsets
        if(!$user->hasRole('recruiter') ) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can update the questionset.
     *
     * @param  \App\User  $user
     * @param  \App\Questionset  $questionset
     * @return mixed
     */
    public function update(User $user, Questionset $questionset)
    {
        // only super-admins and recruiters can update questionsets
        if($user->id == $questionset->user_id) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the questionset.
     *
     * @param  \App\User  $user
     * @param  \App\Questionset  $questionset
     * @return mixed
     */
    public function delete(User $user, Questionset $questionset)
    {
        // Only the super-admin and the owner can deleted
        return $user->id == $questionset->user_id;
    }

}
