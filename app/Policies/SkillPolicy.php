<?php

namespace App\Policies;

use App\Skill;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SkillPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Allow all actions for super-admin skills.
     *
     * @param  \App\Skill  $skill
     * @param  string  $ability
     * @return mixed
     */
    public function before($user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        } else {
            return null; // fall through to the policy method.
        }
    }

    /**
     * Determine whether the skill can view any skills.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //all roles can view skills
        return true;
    }

    /**
     * Determine whether the skill can view the skill.
     *
     * @param  \App\User|null $skill
     * @param  \App\Skill  $skill
     * @return mixed
     */
    public function view(User $user, Skill $skill)
    {

        //all roles can view skills
        return true;
    }

    /**
     * Determine whether the skill can create skills.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        // only super-admin and recruiter can create skills
        if(!$user->hasRole('recruiter')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the skill can update the skill.
     *
     * @param  \App\User  $user
     * @param  \App\Skill  $skill
     * @return mixed
     */
    public function update(User $user, Skill $skill)
    {
        // only super-admin and recruiter can update skills
        if(!$user->hasRole('recruiter')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the skill can delete the skill.
     *
     * @param  \App\User  $user
     * @param  \App\Skill  $skill
     * @return mixed
     */
    public function delete(User $user, Skill $skill)
    {
        // only super-admin and recruiter can delete skills
        if(!$user->hasRole('recruiter')) {
            return false;
        }

        return true;
    }

}
