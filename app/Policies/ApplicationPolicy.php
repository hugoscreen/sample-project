<?php

namespace App\Policies;

use App\User;
use App\Application;
use Illuminate\Auth\Access\HandlesAuthorization;

class ApplicationPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Allow all actions for super-admin users.
     *
     * @param  \App\User  $user
     * @param  string  $ability
     * @return mixed
     */
    public function before($user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        } else {
            return null; // fall through to the policy method.
        }
    }

    /**
     * Determine whether the user can view any companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        // user needs to have read applications permission
        if(!$user->can('read positions') || ( !$user->hasRole('recruiter') && !$user->hasRole('applicant')) ) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can view the application.
     *
     * @param  \App\User|null $user
     * @param  \App\Application  $application
     * @return mixed
     */
    public function view(User $user, Application $application)
    {
        // user needs to have read clients permission
        if(!$user->can('read positions')) {
            return false;
        }

        // applications only can read their own info
        if($user->hasRole('applicant')){
            // applicant only can read applications in which have applicate
            $applicant = $user->applicant;
            if ($applicant->id != $application->applicant_id) {
                return false;
            }
        }

        if($user->hasRole('contractor')){
            //contractor only can read applications that belongs to contractor's company
            $contractor = $user->contractor;
            if ($contractor->company_id != $application->position->company_id) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can create companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        // user needs to have create clients permission
        if(!$user->can('edit positions')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can update the application.
     *
     * @param  \App\User  $user
     * @param  \App\Application  $application
     * @return mixed
     */
    public function update(User $user, Application $application)
    {
        // user needs to have edit clients permission
        if(!$user->can('edit positions')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can delete the application.
     *
     * @param  \App\User  $user
     * @param  \App\Application  $application
     * @return mixed
     */
    public function delete(User $user, Application $application)
    {
        // user needs to have delete clients permission
        if(!$user->can('edit positions')) {
            return false;
        }

        return true;
    }

}
