<?php

namespace App\Policies;

use App\User;
use App\Contractor;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContractorPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Allow all actions for super-admin users.
     *
     * @param  \App\User  $user
     * @param  string  $ability
     * @return mixed
     */
    public function before($user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        } else {
            return null; // fall through to the policy method.
        }
    }

    /**
     * Determine whether the user can view any companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        // user needs to have admin users permission
        if(!$user->hasRole('recruiter')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can view the contractor.
     *
     * @param  \App\User|null $user
     * @param  \App\Contractor  $contractor
     * @return mixed
     */
    public function view(User $user, Contractor $contractor)
    {
        // user needs to have read clients permission
        if(!$user->can('read positions')) {
            return false;
        }

        // contractors can read their own info
        if($user->hasRole('contractor')){
            if($contractor->user_id == $user->id){
                return true;
            }

            if($user->contractor->company_id == $contractor->company_id){
                return true;
            }

            return false;
        }

        // applicant can view contractors on companies that have positions in which applicant has applied
        if($user->hasRole('applicant')){
            $applicant = $user->applicant;
            $authorized = false;
            foreach($applicant->applications as $application){
                if($application->position->company_id == $contractor->company_id) {
                    $authorized = true;
                }
            }

            return $authorized;
        }

        if($user->hasRole('recruiter')){
            return true;
        }

        return true;
    }

    /**
     * Determine whether the user can create companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        // user needs to have create clients permission
        if(!$user->hasRole('recruiter')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can update the contractor.
     *
     * @param  \App\User  $user
     * @param  \App\Contractor  $contractor
     * @return mixed
     */
    public function update(User $user, Contractor $contractor)
    {
        // user needs to have edit clients permission
        if(!$user->hasRole('recruiter')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can delete the contractor.
     *
     * @param  \App\User  $user
     * @param  \App\Contractor  $contractor
     * @return mixed
     */
    public function delete(User $user, Contractor $contractor)
    {
        // user needs to have delete clients permission
        if(!$user->hasRole('recruiter')) {
            return false;
        }

        return true;
    }

}
