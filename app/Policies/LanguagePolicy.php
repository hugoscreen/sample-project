<?php

namespace App\Policies;

use App\Language;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LanguagePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Allow all actions for super-admin languages.
     *
     * @param  \App\User  $user
     * @param  string  $ability
     * @return mixed
     */
    public function before($user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        } else {
            return null; // fall through to the policy method.
        }
    }

    /**
     * Determine whether the language can view any languages.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //all roles can view languages
        return true;
    }

    /**
     * Determine whether the language can view the language.
     *
     * @param  \App\User|null $user
     * @param  \App\Language  $language
     * @return mixed
     */
    public function view(User $user, Language $language)
    {

        //all roles can view languages
        return true;
    }

    /**
     * Determine whether the language can create languages.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        // only super-admin and recruiter can create languages
        if(!$user->hasRole('recruiter')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the language can update the language.
     *
     * @param  \App\Language  $language
     * @param  \App\Language  $language
     * @return mixed
     */
    public function update(User $user, Language $language)
    {
        // only super-admin and recruiter can update languages
        if(!$user->hasRole('recruiter')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the language can delete the language.
     *
     * @param  \App\User  $user
     * @param  \App\Language  $language
     * @return mixed
     */
    public function delete(user $user, Language $language)
    {
        // only super-admin and recruiter can delete languages
        if(!$user->hasRole('recruiter')) {
            return false;
        }

        return true;
    }

}
