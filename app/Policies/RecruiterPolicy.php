<?php

namespace App\Policies;

use App\User;
use App\Recruiter;
use Illuminate\Auth\Access\HandlesAuthorization;

class RecruiterPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Allow all actions for super-admin users.
     *
     * @param  \App\User  $user
     * @param  string  $ability
     * @return mixed
     */
    public function before($user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        } else {
            return null; // fall through to the policy method.
        }
    }

    /**
     * Determine whether the user can view any companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        // user needs to have admin users permission
        if(!$user->can('admin users')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can view the recruiter.
     *
     * @param  \App\User|null $user
     * @param  \App\Recruiter  $recruiter
     * @return mixed
     */
    public function view(User $user, Recruiter $recruiter)
    {
        // user needs to have read clients permission
        if(!$user->can('read positions')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can create companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        // user needs to have create users permission
        if(!$user->can('admin users')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can update the recruiter.
     *
     * @param  \App\User  $user
     * @param  \App\Recruiter  $recruiter
     * @return mixed
     */
    public function update(User $user, Recruiter $recruiter)
    {
        // user needs to have edit recruiters permission
        if(!$user->can('admin users')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can delete the recruiter.
     *
     * @param  \App\User  $user
     * @param  \App\Recruiter  $recruiter
     * @return mixed
     */
    public function delete(User $user, Recruiter $recruiter)
    {
        // user needs to have delete recruiters permission
        if(!$user->can('admin users')) {
            return false;
        }

        return true;
    }

}
