<?php

namespace App\Policies;

use App\User;
use App\State;
use Illuminate\Auth\Access\HandlesAuthorization;

class StatePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Allow all actions for super-admin states.
     *
     * @param  \App\User  $user
     * @param  string  $ability
     * @return mixed
     */
    public function before($user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        } else {
            return null; // fall through to the policy method.
        }
    }

    /**
     * Determine whether the state can view any states.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //all roles can view states
        return true;
    }

    /**
     * Determine whether the state can view the state.
     *
     * @param  \App\User|null $user
     * @param  \App\State  $state
     * @return mixed
     */
    public function view(User $user, State $state)
    {

        //all roles can view states
        return true;
    }

    /**
     * Determine whether the state can create states.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        // only super-admin can create states
        return false;
    }

    /**
     * Determine whether the state can update the state.
     *
     * @param  \App\User  $user
     * @param  \App\State  $state
     * @return mixed
     */
    public function update(User $user, State $state)
    {
        // only super-admin can update states
        return false;
    }

    /**
     * Determine whether the state can delete the state.
     *
     * @param  \App\User  $user
     * @param  \App\State  $state
     * @return mixed
     */
    public function delete(User $user, State $state)
    {
        // only super-admin can delete states
        return false;
    }

}
