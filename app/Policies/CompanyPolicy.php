<?php

namespace App\Policies;

use App\User;
use App\Company;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Allow all actions for super-admin users.
     *
     * @param  \App\User  $user
     * @param  string  $ability
     * @return mixed
     */
    public function before($user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        } else {
            return null; // fall through to the policy method.
        }
    }

    /**
     * Determine whether the user can view any companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        // user needs to have read clients permission
        if(!$user->can('read clients')) {
            return false;
        }
        return true;
    }

    /**
     * Determine whether the user can view the company.
     *
     * @param  \App\User|null $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function view(User $user, Company $company)
    {
        // user needs to have read clients permission
        if(!$user->can('read clients')) {
            return false;
        }

        if($user->hasRole('contractor')){
            // contractor can view only his own company
            if ($user->contractor->company_id != $company->id) {
                return false;
            }
        }

        if($user->hasRole('aplicant')){
            // applicant can only read companies of his position applications
            $applicant = $user->applicant;
            foreach($applicant->applications as $application ) {
                if($application->position->company_id ==  $company->id) {
                    return true;
                }
            }
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can create companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        // user needs to have create clients permission
        if(!$user->can('create clients')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can update the company.
     *
     * @param  \App\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function update(User $user, Company $company)
    {
        // user needs to have edit clients permission
        if(!$user->can('edit clients')) {
            return false;
        }

        return true;
    }

    /**
     * Determine whether the user can delete the company.
     *
     * @param  \App\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function delete(User $user, Company $company)
    {
        // user needs to have delete clients permission
        if(!$user->can('delete clients')) {
            return false;
        }

        return true;
    }

}
