<?php

namespace App\Policies;

use App\User;
use App\VideoListing;
use Illuminate\Auth\Access\HandlesAuthorization;

class VideoListingPolicy
{
    use HandlesAuthorization;

    /**
     * Allow all actions for super-admin users.
     *
     * @param  \App\User  $user
     * @param  string  $ability
     * @return mixed
     */
    public function before($user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        } else {
            return null; // fall through to the policy method.
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        if ($user->hasRole('recruiter')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\VideoListing  $videoListing
     * @return mixed
     */
    public function view(User $user, VideoListing $videoListing)
    {
        if ($user->hasRole('recruiter') && $videoListing->user_id == $user->id) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->hasRole('recruiter')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\VideoListing  $videoListing
     * @return mixed
     */
    public function update(User $user, VideoListing $videoListing)
    {
        if ($user->hasRole('recruiter') && $videoListing->user_id == $user->id) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\VideoListing  $videoListing
     * @return mixed
     */
    public function delete(User $user, VideoListing $videoListing)
    {
        if ($user->hasRole('recruiter') && $videoListing->user_id == $user->id) {
            return true;
        }
        return false;
    }


    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\VideoListing  $videoListing
     * @return mixed
     */
    public function makeShareableLink(User $user, VideoListing $videoListing)
    {
        if ($user->hasRole('recruiter') && $videoListing->user_id == $user->id) {
            return true;
        }
        return false;
    }
}
