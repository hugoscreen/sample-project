<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;

    public function questionset()
    {
        return $this->belongsTo('App\Questionset', 'questionset_id');
    }
}
