<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Document extends Model
{
    //
    public function application()
    {
        return $this->belongsTo('App\Application');
    }

    public function getLink()
    {
        if (Storage::disk('do')->missing($this->getAttribute('name'))) {
            $this->delete();
            return 'File is missing';
        }

        return Storage::disk('do')->temporaryUrl(
            $this->getAttribute('name'), now()->addMinutes(5)
        );
    }
}
