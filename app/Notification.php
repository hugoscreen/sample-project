<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $casts = [
        'read' => 'boolean',
    ];

    // Relationships
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
