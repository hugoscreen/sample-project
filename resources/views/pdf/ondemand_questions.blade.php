<html>
    <head>
        <style type="text/css">
            * {font-family: Helvetica, arial}

            .page_centered{
                width:1000px;
                height:600px;
                position:absolute;
                left:50%;
                top:50%;
                margin:-300px 0 0 -500px;
            }

            .page_break { page-break-before: always; }

            .collokio_logo {
                text-align: center;
            }

            .collokio_logo img {
                width: 150px;
            }

            h1 { font-size: 2em }
            h2 { font-size: 1.7em }
            h3 { font-size: 1.6em }
            h4 { font-size: 1.2em }

            body { font-size: 2em; }
        </style>
    </head>
    <body>
        <div class="page_centered">
            <p class="collokio_logo"><img src="https://collokio.sfo2.cdn.digitaloceanspaces.com/staticElements/logo_negro.png"></p>
            <hr/>
            <h3 style="text-align: center">Position: {{$positionName}}</h3>
            <h1 style="text-align: center">{{$questionset->name}}</h1>
            <h4 style="text-align: center">{{$questionset->instructions}}</h4>
            <div class="page_break"></div>
        </div>


        @foreach($questionset->questions as $question)
            <div class="page_centered">
                <p class="collokio_logo"><img src="https://collokio.sfo2.cdn.digitaloceanspaces.com/staticElements/logo_negro.png"></p>
                <hr/>
                <h1 style="text-align: center">{{$question->question}}</h1>
                <h3 style="text-align: center">{{$question->extra_info}}</h3>
                <div class="page_break"></div>
            </div>
        @endforeach

        <div class="page_centered">
            <p class="collokio_logo"><img src="https://collokio.sfo2.cdn.digitaloceanspaces.com/staticElements/logo_negro.png"></p>
            <hr/>
            <h1 style="text-align: center">Thank you!</h1>
            <h3 style="text-align: center">To finish the Video Interview please click on the three dots at the top right side of the screen and click on “End meeting”.</h3>
        </div>
    </body>
</html>
