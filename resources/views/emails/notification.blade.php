<p>Hello, {{$notification->user->name}}:</p>

<p>You have received a notification from the Collok.io platform.</p>

<p>{!! $notification->message !!}</p>

<p><a href="{{$notification->url}}">{{$notification->calltoaction}}</a></p>
