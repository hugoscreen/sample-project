<?php

use App\Applicant;
use App\Application;
use App\Company;
use App\Contractor;
use App\Position;
use App\Recruiter;
use App\User;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('initDataTest',function (){
   $password = Hash::make('hola');
   $user = new User();
   $user->setAttribute('name', "Admin");
   $user->setAttribute('email', 'admin@mail.com');
   $user->setAttribute('email_verified_at', '2020-01-01');
   $user->setAttribute('password', $password);
   $user->save();

    $user = new User();
    $user->setAttribute('name', "Recruiter");
    $user->setAttribute('email', 'recruiter@mail.com');
    $user->setAttribute('email_verified_at', '2020-01-01');
    $user->setAttribute('password', $password);
    $user->save();

    $user = new User();
    $user->setAttribute('name', "Contractor");
    $user->setAttribute('email', 'contractor@mail.com');
    $user->setAttribute('email_verified_at', '2020-01-01');
    $user->setAttribute('password', $password);
    $user->save();

    $user = new User();
    $user->setAttribute('name', "Applicant");
    $user->setAttribute('email', 'applicant@mail.com');
    $user->setAttribute('email_verified_at', '2020-01-01');
    $user->setAttribute('password', $password);
    $user->save();

    DB::table('model_has_roles')->insert(['role_id'=>4, 'model_type'=>'App\User', 'model_id'=>1]);

    $company = new Company();
    $company->setAttribute('name', 'Company 1');
    $company->setAttribute('phone','4421593974');
    $company->setAttribute('state_id',3);
    $company->setAttribute('city','NY');
    $company->setAttribute('logo','LOGO');
    $company->setAttribute('user_id',1);
    $company->save();

    $recruiter = new Recruiter();
    $recruiter->setAttribute('position', 'Jefe de reclutadores');
    $recruiter->user()->associate(User::find(2));
    $recruiter->save();

    $contractor = new Contractor();
    $contractor->setAttribute('position','CTO');
    $contractor->user()->associate(User::find(3));
    $contractor->save();

    $applicant = new Applicant();
    $applicant->setAttribute('linkedin', 'linkedin');
    $applicant->setAttribute('github', 'github');
    $applicant->setAttribute('stackoverflow', 'stackoverflow');
    $applicant->user()->associate(User::find(4));
    $applicant->save();

    $position = new Position();
    $position->setAttribute('name', 'BE Dev');
    $position->setAttribute('description', 'Back end Deeveloper PHP');
    $position->setAttribute('video', 'VIDEO');
    $position->company()->associate($company);
    $position->save();

    $application = new Application();
    $application->setAttribute('status', 'New');
    $application->applicant()->associate($applicant);
    $application->position()->associate($position);
    $application->save();

    Artisan::call('passport:install');

});
