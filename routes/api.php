<?php

use Illuminate\Support\Facades\Route;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use App\Http\Resources\PublicVideoListing;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::fallback(function(){
    return response()->json(['message' => 'Page Not Found'], 404);
});

Route::middleware('auth:api', 'throttle:60,1')->group(function () {
    Route::apiResources([
        'companies'=>'CompanyController',
        'applicants'=>'ApplicantController',
        'users'=>'UserController',
        'skills'=>'SkillController',
        'languages'=>'LanguageController',
        'countries'=>'CountryController',
        'states'=>'StateController',
        'positions'=>'PositionController',
        'applications'=>'ApplicationController',
        'contractors'=>'ContractorController',
        'recruiters'=>'RecruiterController',
        'questionsets'=>'QuestionsetController',
        'questions'=>'QuestionController',
        'comments'=>'CommentController',
        'live-interview'=>'LiveinterviewController',
        'ondemand-interview'=>'OndemandinterviewController',
        'email_logs'=>'EmailLogController',
        'video_listing'=>'VideoListingController',
        'subscriptions'=>'SubscriptionController',
        'notifications'=>'NotificationController',
    ]);

    Route::get('/get-recordings/{id}','BBBController@getRecordings');
    Route::get('/rehearsal-meeting','BBBController@joinRehearsalMeeting');

    Route::get('/me','UserController@me');
    Route::post('/resend-registration-email','UserController@resendRegistrationEmail');
    Route::get('/interview/{type}/{id}/comments','CommentController@getInterviewComments');
    Route::get('applications/{id}/documents/', 'ApplicationController@getDocuments');
    Route::post('documents','DocumentController@store');
    Route::delete('documents/{id}','DocumentController@destroy');
    Route::post('/questionsets/duplicates','QuestionsetController@duplicates');

    Route::post('/video_listing/{id}/share', 'VideoListingController@makeShareableLink');
    Route::post('/notifications/mark_all_read', 'NotificationController@markAllRead');
});

Route::post('/positions/{id}/apply', 'PositionController@apply');
Route::post('/users/password_reset','UserController@password_reset');

/**
 * Shareable links
 */
Route::get('/shared/video_listing/{shareable_link}', ['middleware' => 'shared', function (ShareableLink $link) {
    return new PublicVideoListing($link->shareable);
}]);
